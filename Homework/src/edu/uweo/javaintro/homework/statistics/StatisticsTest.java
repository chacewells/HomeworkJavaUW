package edu.uweo.javaintro.homework.statistics;

import java.util.Arrays;

public class StatisticsTest {
	
	private static double[] oDataSet = {600,470,170,430,300};
	
	private static double[] eDataSet = {5,7,3,4,6,3,8,9};
	
	private static double[] oneVal = {20};
	
	private static Statistics oStats, eStats, oneValStats;

	public static void main(String[] args) {
		init();
		
		testAll(eStats);
		testAll(oStats);
		testAll(oneValStats);
	}
	
	private static void init() {
		oStats = new Statistics(oDataSet);
		eStats = new Statistics(eDataSet);
		oneValStats = new Statistics(oneVal);
	}
	
	private static void testMean( Statistics stats ) {
		System.out.println( "mean: " + stats.mean() );
	}
	
	private static void testMedian( Statistics stats ) {
		System.out.println( "median: " + stats.median() );
	}
	
	private static void testMinimum( Statistics stats ) {
		System.out.println( "minimum val: " + stats.minimum() );
	}
	
	private static void testMaximum( Statistics stats ) {
		System.out.println( "maximum val: " + stats.maximum() );
	}
	
	private static void testQuartiles( Statistics stats ) {
		int inx = 0;
		for ( double quartile : stats.quartiles() ) {
			System.out.printf( "Quartile %d: %.2f%n", ++inx, quartile );
		}
	}
	
	private static void testVariance( Statistics stats ) {
		System.out.println( "variance: " + stats.variance() );
	}
	
	private static void testStandardDeviation( Statistics stats ) {
		System.out.println( "std dev: " + stats.standardDeviation() );
	}
	
	private static void testMaxAbsDev( Statistics stats ) {
		System.out.println( "max abs dev: " + stats.maximumAbsoluteDeviation() );
	}
	
	private static void testMedAbsDev( Statistics stats ) {
		System.out.println( "med abs dev: " + stats.medianAbsoluteDeviation() );
	}
	
	private static void testMeanAbsDev( Statistics stats ) {
		System.out.println( "mean abs dev: " + stats.meanAbsoluteDeviation() );
	}
	
	private static void testGetSamples( Statistics stats ) {
		System.out.print( "samples: " );
		for ( double d : stats.getSamples() )
			System.out.printf( "%.2f ", d );
		System.out.println();
	}
	
	private static void testGetSamplesOrdered( Statistics stats ) {
		System.out.print( "samples: " );
		double[] samplesOrdered = stats.getSamples();
		Arrays.sort(samplesOrdered);
		
		for ( double d : samplesOrdered )
			System.out.printf( "%.2f ", d );
		System.out.println();
	}
	
	private static void testAll( Statistics stats ) {
		testGetSamplesOrdered(stats);
		testMean(stats);
		testMedian(stats);
		testMinimum(stats);
		testMaximum(stats);
		testQuartiles(stats);
		testVariance(stats);
		testStandardDeviation(stats);
		testMaxAbsDev(stats);
		testMedAbsDev(stats);
		testMeanAbsDev(stats);
		System.out.println();
	}
	
}
