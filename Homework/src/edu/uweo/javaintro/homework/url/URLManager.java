package edu.uweo.javaintro.homework.url;

/**
 * Provides generic getters and setters for subclasses managing a URL.
 * @author chq-aaronwe
 *
 */
public abstract class URLManager {
	
	public static final String	PROTOCOL_DELIM = "://",
								PORT_DELIM = ":",
								PATH_DELIM = "/",
								QUERY_DELIM = "?",
								FRAGMENT_DELIM = "#";
	
	private		String		protocol,
							domain,
							path,
							fragmentID;
	private		int			port = -1;
	
	/**
	 * Returns the receiver's protocol element.
	 * @return the protocol for this receiver.
	 */
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	/**
	 * Returns the receiver's domain element.
	 * @return the domain for this receiver.
	 */
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	/**
	 * Returns the receiver's path element.
	 * @return the path for this receiver.
	 */
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * Returns the receiver's fragment id element.
	 * @return the fragment id for this receiver.
	 */
	public String getFragmentID() {
		return fragmentID;
	}
	public void setFragmentID(String fragmentID) {
		this.fragmentID = fragmentID;
	}
	/**
	 * Returns the receiver's port number.
	 * @return the port number for this receiver.
	 */
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	
	/**
	 * Returns this receiver's raw query string
	 * @return the receiver's raw query string
	 */
	public abstract String getQueryString();
	
	protected boolean hasProtocol() {
		return getProtocol() != null;
	}
	protected boolean hasDomain() {
		return getDomain() != null;
	}
	protected boolean hasPath() {
		String path = getPath();
		return path != null && !path.equals("") && !path.equals("/");
	}
	protected boolean hasFragmentId() {
		return getFragmentID() != null;
	}
	protected boolean hasPort() {
		return port > -1;
	}

}
