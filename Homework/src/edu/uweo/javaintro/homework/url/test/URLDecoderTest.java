package edu.uweo.javaintro.homework.url.test;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import edu.uweo.javaintro.homework.url.URLDecoder;

public class URLDecoderTest {
	public static final String		URL_FULLY_QUALIFIED =					"file://mfg.acme.com:81/base/input/list.txt?a=b&c=d#GROCERY";
	public static final String		URL_FULLY_QUALIFIED_ENCODED_QUERY	=	"file://mfg.acme.com:81/base/input/list.txt?hello=it%27s+nice+out%21&a=b#GROCERY";

	public static final String		URL_DOMAIN = 						"mfg.acme.com";
	public static final String		URL_DOMAIN_PATH = 					"mfg.acme.com/base/input/list.txt";
	public static final String		URL_DOMAIN_PATH_QUERY = 			"mfg.acme.com/base/input/list.txt?a=b&c=d";
	public static final String		URL_DOMAIN_PATH_QUERY_FRAGMENTID = 	"mfg.acme.com/base/input/list.txt?a=b&c=d#GROCERY";
	
	public static final String		URL_PROTOCOL_DOMAIN = 				"file://mfg.acme.com";
	public static final String		URL_PROTOCOL_DOMAIN_PATH = 			"file://mfg.acme.com/base/input/list.txt";
	public static final String		URL_PROTOCOL_DOMAIN_PATH_QUERY = 	"file://mfg.acme.com/base/input/list.txt?a=b&c=d";
	
	public static final String		URL_PROTOCOL_DOMAIN_PORT = 							"file://mfg.acme.com:81";
	public static final String		URL_PROTOCOL_DOMAIN_PORT_PATH = 					"file://mfg.acme.com:81/base/input/list.txt";
	public static final String		URL_PROTOCOL_DOMAIN_PORT_PATH_QUERY = 				"file://mfg.acme.com:81/base/input/list.txt?a=b&c=d";
	public static final String		URL_PROTOCOL_DOMAIN_PORT_PATH_QUERY_FRAGMENTID =	"file://mfg.acme.com:81/base/input/list.txt?a=b&c=d#GROCERY";
	
	public static final String		URL_DOMAIN_PORT_QUERY =						"mfg.acme.com:81/?a=b&c=d";
	public static final String		URL_DOMAIN_PORT_QUERY_FRAGMENTID =			"mfg.acme.com:81/?a=b&c=d#GROCERY";
	public static final String		URL_PROTOCOL_DOMAIN_PORT_QUERY = 			"file://mfg.acme.com:81/?a=b&c=d";
	public static final String		URL_PROTOCOL_DOMAIN_PORT_QUERY_FRAGMENTID =	"file://mfg.acme.com:81/?a=b&c=d#GROCERY";
	public static final String		URL_DOMAIN_QUERY =							"mfg.acme.com/?a=b&c=d";
	public static final String		URL_DOMAIN_QUERY_FRAGMENTID =				"mfg.acme.com/?a=b&c=d#GROCERY";
	public static final String		URL_PROTOCOL_DOMAIN_QUERY_FRAGMENT_ID =		"file://mfg.acme.com:81/?a=b&c=d#GROCERY";

	public static final String		URL_DOMAIN_PORT_FRAGMENT_ID =				"mfg.acme.com:81/#GROCERY";
	public static final String		URL_PROTOCOL_DOMAIN_PORT_FRAGMENT_ID =		"file://mfg.acme.com:81/#GROCERY";
	
	public static final String		EXPECTED_NO_ENCODING_PROTOCOL 		= 		"file";
	public static final String		EXPECTED_NO_ENCODING_DOMAIN 		= 		"mfg.acme.com";
	public static final Integer		EXPECTED_NO_ENCODING_PORT			= 		81;
	public static final String		EXPECTED_NO_ENCODING_PATH			= 		"base/input/list.txt";
	public static final String		EXPECTED_NO_ENCODING_QUERY_STRING	= 		"a=b&c=d";
	public static final String		EXPECTED_NO_ENCODING_FRAGMENT_ID	=		"GROCERY";
	public static final String		EXPECTED_QUERY_STRING_ENCODED		=		"hello=it%27s+nice+out%21&a=b";
	public static final String		EXPECTED_QUERY_STRING_DECODED		=		"hello=it's nice out!&a=b";
	public static final Map<String,String> EXPECTED_QUERY_MAP_DECODED	=		new HashMap<>();
	static {
		EXPECTED_QUERY_MAP_DECODED.put("hello", "it's nice out!");
		EXPECTED_QUERY_MAP_DECODED.put("a", "b");
	}

	@Test
	public void urlDecoderConstructor() {
		URLDecoder decoder = new URLDecoder( URL_FULLY_QUALIFIED );
		assertEquals( decoder.toString(), decoder.toString() );
	}
	
	@Test
	public void decodeFullyQualifiedGetProtocol() {
		URLDecoder d = new URLDecoder( URL_FULLY_QUALIFIED );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PROTOCOL, d.getProtocol() );
	}
	
	@Test
	public void decodeFullyQualifiedGetDomain() {
		URLDecoder d = new URLDecoder( URL_FULLY_QUALIFIED );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_DOMAIN, d.getDomain() );
	}
	
	@Test
	public void decodeFullyQualifiedGetPort() {
		URLDecoder d = new URLDecoder( URL_FULLY_QUALIFIED );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PORT, (Integer)d.getPort() );
	}
	
	@Test
	public void decodeFullyQualifiedGetPath() {
		URLDecoder d = new URLDecoder( URL_FULLY_QUALIFIED );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PATH, d.getPath() );
	}
	
	@Test
	public void decodeFullQualifiedGetQueryString() {
		URLDecoder d = new URLDecoder( URL_FULLY_QUALIFIED );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_QUERY_STRING, d.getQueryString() );
	}
	
	@Test
	public void decodeFullyQualifiedGetFragmendID() {
		URLDecoder d = new URLDecoder( URL_FULLY_QUALIFIED );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_FRAGMENT_ID, d.getFragmentID() );
	}
	
	@Test
	public void decodeDomainGetProtocol() {
		URLDecoder d = new URLDecoder( URL_DOMAIN );
		d.decode();
		
		assertEquals( null, d.getProtocol() );
	}
	
	@Test
	public void decodeDomainGetDomain() {
		URLDecoder d = new URLDecoder( URL_DOMAIN );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_DOMAIN, d.getDomain() );
	}
	
	@Test
	public void decodeDomainPathGetDomain() {
		URLDecoder d = new URLDecoder( URL_DOMAIN_PATH );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_DOMAIN, d.getDomain() );
	}
	
	@Test
	public void decodeDomainPathGetPath() {
		URLDecoder d = new URLDecoder( URL_DOMAIN_PATH );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PATH, d.getPath() );
	}
	
	@Test
	public void decodeDomainPathQueryGetDomain() {
		URLDecoder d = new URLDecoder( URL_DOMAIN_PATH );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PATH, d.getPath() );
	}
	
	@Test
	public void decodeDomainPathQueryGetPath() {
		URLDecoder d = new URLDecoder( URL_DOMAIN_PATH_QUERY );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PATH, d.getPath() );
	}
	
	@Test
	public void decodeDomainPathQueryGetQueryString() {
		URLDecoder d = new URLDecoder( URL_DOMAIN_PATH_QUERY );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_QUERY_STRING, d.getQueryString() );
	}
	
	@Test
	public void decodeDomainPathQueryFragmentIDGetProtocol() {
		URLDecoder d = new URLDecoder( URL_DOMAIN_PATH_QUERY );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_QUERY_STRING, d.getQueryString() );
	}
	
	@Test
	public void decodeDomainPathQueryFragmentIDGetDomain() {
		URLDecoder d = new URLDecoder( URL_DOMAIN_PATH_QUERY_FRAGMENTID );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_QUERY_STRING, d.getQueryString() );
	}
	
	@Test
	public void decodeDomainPathQueryFragmentIDGetPath() {
		URLDecoder d = new URLDecoder( URL_DOMAIN_PATH_QUERY_FRAGMENTID );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PATH, d.getPath() );
	}
	
	@Test
	public void decodeDomainPathQueryFragmentIDGetQueryString() {
		URLDecoder d = new URLDecoder( URL_DOMAIN_PATH_QUERY_FRAGMENTID );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_QUERY_STRING, d.getQueryString() );
	}
	
	@Test
	public void decodeDomainPathQueryFragmentIDGetFragmentID() {
		URLDecoder d = new URLDecoder( URL_DOMAIN_PATH_QUERY_FRAGMENTID );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_FRAGMENT_ID, d.getFragmentID() );
	}
	
	@Test
	public void decodeProtocolDomainGetProtocol() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PROTOCOL, d.getProtocol() );
	}
	
	@Test
	public void decodeProtocolDomainGetDomain() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_DOMAIN, d.getDomain() );
	}
	
	@Test
	public void decodeProtocolDomainPathGetProtocol() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PATH );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PROTOCOL, d.getProtocol() );
	}
	
	@Test
	public void decodeProtocolDomainPathQueryDecode() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PATH );
		d.decode();
		
		assertEquals( (Map<String,String>)(new HashMap<String,String>()), d.getQueryMap() );
	}
	
	@Test
	public void decodeProtocolDomainPathGetDomain() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PATH );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_DOMAIN, d.getDomain() );
	}
	
	@Test
	public void decodeProtocolDomainPathGetPort() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PATH );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PATH, d.getPath() );
	}
	
	@Test
	public void decodeProtocolDomainPathGetPath() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PATH );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PATH, d.getPath() );
	}
	
	@Test
	public void decodeProtocolDomainPathQueryGetProtocol() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PATH_QUERY );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PROTOCOL, d.getProtocol() );
	}
	
	@Test
	public void decodeProtocolDomainPathQueryGetDomain() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PATH_QUERY );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_DOMAIN, d.getDomain() );
	}
	
	@Test
	public void decodeProtocolDomainPathQueryGetPort() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PATH_QUERY );
		d.decode();
		
		assertEquals( -1, (int)d.getPort() );
	}
	
	@Test
	public void decodeProtocolDomainPathQueryGetPath() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PATH_QUERY );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PATH, d.getPath() );
	}
	
	@Test
	public void decodeProtocolDomainPathQueryGetQueryString() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PATH_QUERY );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_QUERY_STRING, d.getQueryString() );
	}
	
	@Test
	public void decodeProtocolDomainPortGetProtocol() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PORT );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PROTOCOL, d.getProtocol() );
	}
	
	@Test
	public void decodeProtocolDomainPortGetDomain() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PORT );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_DOMAIN, d.getDomain() );
	}
	
	@Test
	public void decodeProtocolDomainPortGetPort() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PORT );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PORT, (Integer)d.getPort() );
	}
	
	@Test
	public void decodeProtocolDomainPortPathGetProtocol() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PORT_PATH );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PORT, (Integer)d.getPort() );
	}
	
	@Test
	public void decodeProtocolDomainPortPathGetDomain() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PORT_PATH );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_DOMAIN, d.getDomain() );
	}
	
	@Test
	public void decodeProtocolDomainPortPathGetPort() {
		URLDecoder d = new URLDecoder( URL_PROTOCOL_DOMAIN_PORT_PATH );
		d.decode();
		
		assertEquals( EXPECTED_NO_ENCODING_PORT, (Integer)d.getPort() );
	}
	
	@Test
	public void decodeFullyQualifiedGetQueryStringEncoded() {
		URLDecoder d = new URLDecoder( URL_FULLY_QUALIFIED_ENCODED_QUERY );
		d.decode();
		
		assertEquals( EXPECTED_QUERY_STRING_ENCODED, d.getQueryString() );
	}
	
	@Test
	public void decodeFullyQualifiedGetQueryMapDecoded() {
		URLDecoder d = new URLDecoder( URL_FULLY_QUALIFIED_ENCODED_QUERY );
		d.decode();
		
		assertEquals( EXPECTED_QUERY_MAP_DECODED, d.getQueryMap() );
	}
	
}
