package edu.uweo.java2.homework.threads;

import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Provides a client that obtains tasks from a task server and executes
 * them in a multithreaded fashion. The task provider for this client is
 * the class TaskGenerator.
 * @author chq-aaronwe
 *
 */
public class TaskThreader {
	private boolean debug;
	private int numThreads;
	private AtomicInteger taskCount = new AtomicInteger();
	private AtomicInteger threadsDone = new AtomicInteger();
	
	private final Runnable taskRunnable = new Runnable() {

		@Override
		public void run() {
			Thread currentThread = Thread.currentThread();
			
			try {
				for (Task task = TaskGenerator.nextTask();
						!currentThread.isInterrupted();
						task.execute(),
						debugTaskInfo(task),
						task = TaskGenerator.nextTask());
			} catch (IllegalTaskStateException e) {
				System.out.println("IllegalTaskStateException: " + 
						"task #" + e.getTask().getIdent() + 
						", '" + e.getMessage() + "'");
				e.printStackTrace();
				System.exit(1);
			}
			
			debugThreadDone();
		}
		
		private void debugTaskInfo(Task task) {
			if (debug) {
				System.out.println("Task " + task.getIdent() + " started " +
						task.getStartTime() + " and ended " +
						task.getEndTime() + " on " +
						Thread.currentThread().getName());
				
			}
			taskCount.incrementAndGet();
		}
		
		private void debugThreadDone() {
			if (debug) System.out.println(Thread.currentThread().getName() + " all done");
			threadsDone.incrementAndGet();
		}
		
	};
	
	/**
	 * Initializes an instance wherein tasks will be executed using the
	 * provided number of threads.
	 * @param numThreads
	 */
	public TaskThreader(int numThreads) {
		this.numThreads = numThreads;
	}
	
	/**
	 * Sets debug mode for this receiver.
	 * @param debug <b>true</b> for debug mode; <b>false</b> for no debug.
	 */
	void setDebug(boolean debug) {
		this.debug = debug;
	}
	
	/**
	 * Executes TaskGenerator provided tasks with as many threads as indicated
	 * in this receiver's constructor. Tasks will continue execution 
	 * indefinitely. This is a blocking method that awaits a call to
	 * TaskGenerator.initiateShutdown(), at which point each thread will
	 * finish its current task and exit. Returns once all threads have finished.
	 */
	public void process() {
		Collection<Thread> threads = new LinkedList<>();
		
		for ( int inx = 0; inx < numThreads; inx++ ) {
			Thread thread = new Thread(taskRunnable, "thread " + inx);
			thread.start();
			threads.add(thread);
		}
		
		TaskGenerator.waitForShutdown();
		
		for (Thread thread : threads)
			thread.interrupt();
		
		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {}
		}
		
		debugAllThreadsAndTasksCompleted();
	}
	
	private void debugAllThreadsAndTasksCompleted() {
		if (debug) {
			allThreadsAndTasksCompleted();
		}
	}
	
	void allThreadsAndTasksCompleted() {
		System.out.println(threadsDone + " threads have finished");
		System.out.println(taskCount + " tasks completed");
	}
	
}
