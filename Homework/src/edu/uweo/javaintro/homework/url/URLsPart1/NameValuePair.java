package edu.uweo.javaintro.homework.url.URLsPart1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class NameValuePair {
	
	private String	name,
					value;
	
	public NameValuePair() {
		name = value = "";
	}
	
	public NameValuePair( String name, String value ) {
		this.name = name;
		this.value = value;
	}
	
	public static NameValuePair getInstance( String pairedString ) {
		NameValuePair pair = null;
		
		try {
			String[] divided = pairedString.split("=", 2);
			pair = new NameValuePair(divided[0], divided[1]);
		} catch ( IndexOutOfBoundsException e ) {
			throw new IllegalArgumentException( "Could not parse name-value pair" );
		}
		
		return pair;
	}
	
	public static List<NameValuePair> getList( NameValuePair[] pairs ) {
		return new ArrayList<NameValuePair>( Arrays.asList(pairs) );
	}
	
	public static List<NameValuePair> getList( Map<String,String> map ) {
		List<NameValuePair> list = new ArrayList<NameValuePair>( map.size() );
		Set<String> names = map.keySet();
		
		for ( String name : names ) {
			String value = map.get(name);
			list.add( new NameValuePair(name, value) );
		}
		
		return list;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return name + '=' + value;
	}
	
}
