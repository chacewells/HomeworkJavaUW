package edu.uweo.javaintro.homework.url.URLsPart1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

public class URLQueryDecoder {
	
	Map<String,String> queryMap;
	
	private String toDecode;

	public URLQueryDecoder(String toDecode) {
		this.toDecode = toDecode;
		queryMap = new HashMap<String,String>();
	}

//	returns a map of property names with their values
//	as decoded from 'toDecode' field
	public Map<String, String> decode() {
		
		StringTokenizer tizer = new StringTokenizer(toDecode, "&");
		List<NameValuePair> pairs = new ArrayList<>();
		
		while ( tizer.hasMoreTokens() ) {
			pairs.add( NameValuePair.getInstance( tizer.nextToken() ) );
		}
		
		for ( NameValuePair pair : pairs ) {
			String name = decode(pair.getName());
			String value = decode(pair.getValue());
			queryMap.put(name, value);
		}
		
		return queryMap;
	}
	
//	returns a decoded version of 'toDecode' field
	private static String decode( String str ) throws RuntimeException {
		StringBuilder sb = new StringBuilder();
		for ( int inx = 0; inx < str.length(); inx++ ) {
//			try to capture an encoded character
			char c = str.charAt(inx);
			try {
				if ( c == '%' ) {
					sb.append( (char)Byte.parseByte( str.substring( inx+1, inx+3), 16) );
					inx += 2;
				} else
					sb.append( ( c == '+' ) ? ' ' : str.charAt(inx) );
			} catch ( Exception e ) {
				if ( e instanceof IndexOutOfBoundsException || e instanceof NumberFormatException )
					throw new IllegalArgumentException( "Invalid character code"
							+ " detected at string index: " + inx + "; "
							+ "substring: " 
							+ ( str.length() > inx+2 
									? str.substring(inx, inx+3) 
											: str.substring(inx) ) );
				else
					throw new RuntimeException(e);
			}
		}
		
		return sb.toString();
	}
	
//	returns a map
//	the map contains elements decoded from the 'toDecode' field
//	the map is empty otherwise
	public Map<String, String> getQueryMap() {
		return queryMap;
	}

//	returns a list of NameValuePair objects decoded from 'toDecode' field
	public List<NameValuePair> getQueryList() {
		List<NameValuePair> nvpList = new ArrayList<NameValuePair>();
		Set<String> keySet = decode().keySet();
		
		for ( String key : keySet )
			nvpList.add( new NameValuePair( key, queryMap.get(key) ) );
		
		return new ArrayList<NameValuePair>(nvpList);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		Iterator<NameValuePair> itr = getQueryList().iterator();
		
		while ( itr.hasNext() )
			sb.append( itr.next().toString() )
			.append( itr.hasNext() ? '&' : "" );
		
		return sb.toString();
	}
	
}
