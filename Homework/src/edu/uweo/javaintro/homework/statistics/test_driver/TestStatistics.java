package edu.uweo.javaintro.homework.statistics.test_driver;

public class TestStatistics {
	
	private static TestCaseStatistics[] testCases = {
		new TestCaseStatistics.TestCaseBuilder(new double[]{1.,2.})
		.maximum(2.)
		.maximumAbsoluteDeviation(.5)
		.mean(1.5)
		.meanAbsoluteDeviation(.5)
		.median(1.5)
		.medianAbsoluteDeviation(.5)
		.minimum(1.)
		.range(1.)
		.standardDeviation(.5)
		.firstQuartile(1.)
		.secondQuartile(1.5)
		.thirdQuartile(2.)
		.variance(.25)
		.build()
		,
		new TestCaseStatistics.TestCaseBuilder(new double[]{0,5,10,15,20})
		.maximum(20)
		.maximumAbsoluteDeviation(10)
		.mean(10)
		.meanAbsoluteDeviation(6)
		.median(10)
		.medianAbsoluteDeviation(5)
		.minimum(0)
		.range(20)
		.standardDeviation(7.071068)
		.firstQuartile(2.5)
		.secondQuartile(10)
		.thirdQuartile(17.5)
		.variance(50)
		.build()
		,
//		new TestCaseStatistics.TestCaseBuilder(new double[]{})
//		.maximum(0)
//		.maximumAbsoluteDeviation(0)
//		.mean(Double.NaN)
//		.meanAbsoluteDeviation(0)
//		.median(0)
//		.medianAbsoluteDeviation(0)
//		.minimum(0)
//		.range(0)
//		.standardDeviation(0)
//		.firstQuartile(0)
//		.thirdQuartile(0)
//		.variance(0)
//		.build()
//		,
		new TestCaseStatistics.TestCaseBuilder(new double[]{0,1})
		.maximum(1)
		.maximumAbsoluteDeviation(.5)
		.mean(.5)
		.meanAbsoluteDeviation(.5)
		.median(.5)
		.medianAbsoluteDeviation(.5)
		.minimum(0)
		.range(1)
		.standardDeviation(.5)
		.firstQuartile(0)
		.secondQuartile(.5)
		.thirdQuartile(1)
		.variance(.25)
		.build()
	};
	
	public static void main(String[] args) {
		for ( TestCaseStatistics testCase : testCases )
			testCase.test();
	}

}
