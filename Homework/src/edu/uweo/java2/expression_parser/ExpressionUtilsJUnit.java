package edu.uweo.java2.expression_parser;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ExpressionUtilsJUnit {
	
	static final String FIVE_PLUS_FIVE = "5+5";
	static final String BIG_PLUS_BIG = "0123456789+0123456789";
	static final String FIVE_POINT_FIVE = "5.5";
	static final String FIVE_POINT_FIVE_POINT_FIVE = "5.5.5";
	static final String FIFTEEN_PLUS_FIVE_HEX = "0xF + 0x5";
	static final String THOUSAND_PLUS_THOUSAND_HEX = "0x3E8 + 0X3e8";
	static final String SYMBOL_PLUS_SYMBOL = "fight = cat + dog";
	static final String SYMBOL_PLUS_FIVE = "finger + 5";
	static final String SYMBOL_STRING = "these_are_symbols_9yay";
	static final String FIVE_PLUS_TWO_BINARY = "0b101 - 0B010";
	static final String Q_MARK = "?";
	static final String FUNCTION_WITH_SYMBOLIC_ARGS = "funcThis( arg1, arg2 )";
	static final String FUNCTION_NESTED = "funky(motherFunker()+5)";
	static final String CRAPPY_FUNCTION = "5function()";
	static final String CRAPPY_FUNCTION_2 = "function(";

	@Test
	public void isHexCharDecimal() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_PLUS_FIVE);
		int index = 0;
		char test = obj.getCharacterExpression()[index];
		boolean expected = true;
		
		assertEquals(expected, ExpressionUtils.isHexChar(test));
	}
	
	@Test
	public void isSymbolCharTrue() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_PLUS_FIVE);
		int index = 0;
		char test = obj.getCharacterExpression()[index];
		boolean expected = true;
		
		assertEquals(expected, ExpressionUtils.isSymbolChar(test));
	}
	
	@Test
	public void isSymbolCharFalse() {
		ExpressionUtils obj = new ExpressionUtils(Q_MARK);
		int index = 0;
		char test = obj.getCharacterExpression()[index];
		boolean expected = false;
		
		assertEquals(expected, ExpressionUtils.isSymbolChar(test));
	}
	
	@Test
	public void isBinaryCharTrue() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_PLUS_TWO_BINARY);
		int index = 2;
		char test = obj.getCharacterExpression()[index];
		boolean expected = true;
		
		assertEquals(expected, ExpressionUtils.isBinaryChar(test));
	}
	
	@Test
	public void isBinaryCharFalse() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_PLUS_FIVE);
		int index = 0;
		char test = obj.getCharacterExpression()[index];
		boolean expected = false;
		
		assertEquals(expected, ExpressionUtils.isBinaryChar(test));
	}
	
	@Test
	public void isDecimalValueTrue() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_PLUS_FIVE);
		int index = 0;
		boolean expected = true;
		
		assertEquals(expected, obj.isDecimalValue(index));
	}
	
	@Test
	public void isDecimalValueTrueEndOfString() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_PLUS_FIVE);
		int index = 2;
		boolean expected = true;
		
		assertEquals(expected, obj.isDecimalValue(index));
	}
	
	@Test
	public void isBinaryValueTrue() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_PLUS_TWO_BINARY);
		int index = 6;
		boolean expected = true;
		
		assertEquals(expected, obj.isBinaryValue(index));
	}
	
	@Test
	public void isBinaryValueFalse() {
		ExpressionUtils obj = new ExpressionUtils(FIFTEEN_PLUS_FIVE_HEX);
		int index = 0;
		boolean expected = false;
		
		assertEquals(expected, obj.isBinaryValue(index));
	}
	
	@Test
	public void isSymbolValueTrue() {
		ExpressionUtils obj = new ExpressionUtils(SYMBOL_PLUS_SYMBOL);
		int index = 0;
		boolean expected = true;
		
		assertEquals(expected, obj.isSymbolValue(index));
	}
	
	@Test
	public void isSymbolValueOneChar() {
		ExpressionUtils obj = new ExpressionUtils("a");
		int index = 0;
		boolean expected = true;
		assertEquals(expected, obj.isSymbolValue(index));
	}
	
	@Test
	public void isSymbolValueFalse() {
		ExpressionUtils obj = new ExpressionUtils(SYMBOL_PLUS_SYMBOL);
		int index = 5;
		boolean expected = false;
		
		assertEquals(expected, obj.isSymbolValue(index));
	}
	
	@Test
	public void isFunctionValueTrue() {
		ExpressionUtils obj = new ExpressionUtils(FUNCTION_WITH_SYMBOLIC_ARGS);
		int index = 0;
		boolean expected = true;
		
		assertEquals(expected, obj.isFunctionValue(index));
	}
	
	@Test
	public void isFunctionValueFalse() {
		ExpressionUtils obj = new ExpressionUtils(CRAPPY_FUNCTION);
		int index = 0;
		boolean expected = false;
		
		assertEquals(expected, obj.isFunctionValue(index));
	}
	
	@Test
	public void findClosingParenthesisExists() {
		ExpressionUtils obj = new ExpressionUtils(FUNCTION_WITH_SYMBOLIC_ARGS);
		int index = 8;
		int expected = obj.getCharacterExpression().length - 1;
		
		assertEquals(expected, obj.findClosingParenthesis(index));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void findClosingParenthesisInvalidArgument() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_PLUS_FIVE);
		int index = 0;
		obj.findClosingParenthesis(index);
	}
	
	@Test
	public void findClosingParenthesisNoClosing() {
		ExpressionUtils obj = new ExpressionUtils(CRAPPY_FUNCTION_2);
		int index = 8;
		int expected = CRAPPY_FUNCTION_2.length();
		
		assertEquals(expected, obj.findClosingParenthesis(index));
	}
	
	@Test
	public void findClosingParenthesisNestedOuter() {
		ExpressionUtils obj = new ExpressionUtils(FUNCTION_NESTED);
		int index = 5;
		int expected = FUNCTION_NESTED.length() - 1;
		
		assertEquals(expected, obj.findClosingParenthesis(index));
	}
	
	@Test
	public void findClosingParenthesisNestedInner() {
		ExpressionUtils obj = new ExpressionUtils(FUNCTION_NESTED);
		int index = 18;
		int expected = 19;
		
		assertEquals(expected, obj.findClosingParenthesis(index));
	}
	
	@Test
	public void findLastSymbolCharExists() {
		ExpressionUtils obj = new ExpressionUtils(SYMBOL_PLUS_SYMBOL);
		int index = 0;
		int expected = 4;
		
		assertEquals(expected, obj.findLastSymbolChar(index));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void findLastSymbolCharException() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_PLUS_FIVE);
		int index = 0;
		obj.findLastSymbolChar(index);
	}
	
	@Test
	public void findLastSymbolCharEndOfString() {
		ExpressionUtils obj = new ExpressionUtils(SYMBOL_STRING);
		int index = 0;
		int expected = SYMBOL_STRING.length() - 1;
		
		assertEquals(expected, obj.findLastSymbolChar(index));
	}
	
	@Test
	public void findLastDecimalDigitExists() {
		ExpressionUtils obj = new ExpressionUtils(BIG_PLUS_BIG);
		int index = 0;
		int expected = 9;
		
		assertEquals(expected, obj.findLastDecimalDigit(index));
	}
	
	@Test
	public void findLastDecimalDigitEndOfString() {
		ExpressionUtils obj = new ExpressionUtils(BIG_PLUS_BIG);
		int index = 11;
		int expected = BIG_PLUS_BIG.length() - 1;
		
		assertEquals(expected, obj.findLastDecimalDigit(index));
	}
	
	@Test
	public void findLastDecimalDigitDot() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_POINT_FIVE);
		int index = 0;
		int expected = FIVE_POINT_FIVE.length() - 1;
		
		assertEquals(expected, obj.findLastDecimalDigit(index));
	}
	
	@Test
	public void findLastDecimalDigitTwoDots() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_POINT_FIVE_POINT_FIVE);
		int index = 0;
		int expected = FIVE_POINT_FIVE.length() - 1;
		
		assertEquals(expected, obj.findLastDecimalDigit(index));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void findLastDecimalDigitException() {
		ExpressionUtils obj = new ExpressionUtils(SYMBOL_PLUS_FIVE);
		int index = 0;
		obj.findLastDecimalDigit(index);
	}
	
	@Test
	public void findLastBinaryDigitExists() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_PLUS_TWO_BINARY);
		int index = 0;
		int expected = 4;
		
		assertEquals(expected, obj.findLastBinaryDigit(index));
	}
	
	@Test
	public void findLastBinaryDigitEndOfString() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_PLUS_TWO_BINARY);
		int index = 6;
		int expected = FIVE_PLUS_TWO_BINARY.replaceAll("\\s","").length() - 1;
		
		assertEquals(expected, obj.findLastBinaryDigit(index));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void findLastBinaryDigitException() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_PLUS_FIVE);
		int index = 0;
		obj.findLastBinaryDigit(index);
	}
	
	@Test
	public void findLastHexDigitExists() {
		ExpressionUtils obj = new ExpressionUtils(THOUSAND_PLUS_THOUSAND_HEX);
		int index = 0;
		int expected = 4;
		
		assertEquals(expected, obj.findLastHexDigit(index));
	}
	
	@Test
	public void findLastHexDigitEndOfString() {
		ExpressionUtils obj = new ExpressionUtils(THOUSAND_PLUS_THOUSAND_HEX);
		int index = 6;
		int expected = obj.getCharacterExpression().length - 1;
		
		assertEquals(expected, obj.findLastHexDigit(index));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void findLastHexDigitException() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_PLUS_FIVE);
		int index = 0;
		obj.findLastHexDigit(index);
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void isDecimalValueException() {
		ExpressionUtils obj = new ExpressionUtils(FIVE_PLUS_FIVE);
		int index = FIVE_PLUS_FIVE.length() + 1;
		obj.isDecimalValue(index);
	}
	
}
