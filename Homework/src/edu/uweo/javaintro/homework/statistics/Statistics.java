package edu.uweo.javaintro.homework.statistics;

import java.util.Arrays;

public class Statistics {

	private double[] dataSet;

	public Statistics( double[] dataSet ) {
		this.dataSet = Arrays.copyOf(dataSet, dataSet.length);
	}
	
	/**
	 * @return Returns the mean of the dataset.
	 */
	public double mean() {
		return average( dataSet );
	}
	
	/**
	 * @return Returns the median of the dataset.
	 */
	public double median() {
		return median( dataSet );
	}
	
	/**
	 * @return Returns the minimum value of the dataset.
	 */
	public double minimum() {
		double[] dataSet = Arrays.copyOf(this.dataSet, this.dataSet.length);
		Arrays.sort(dataSet);
		return dataSet[0];
	}
	
	/**
	 * @return Returns the maximum value of the dataset.
	 */
	public double maximum() {
		double[] dataSet = Arrays.copyOf(this.dataSet, this.dataSet.length);
		Arrays.sort(dataSet);
		return dataSet[ dataSet.length - 1 ];
	}
	
	/**
	 * @return Returns the absolute value of the difference between minimum and
	 * maximum values of the data set.
	 */
	public double range() {
		double[] set = Arrays.copyOf(dataSet, dataSet.length);
		Arrays.sort(set);
		double distance = Math.abs( set[set.length-1] - set[0] );
		
		return distance;
	}
	
	/**
	 * 
	 * @return Returns an array of three doubles representing the first,
	 * second (median) and third quartiles in elements 0, 1, and 2,
	 * respectively. The calculation of quartiles uses the Moore and McCabe
	 * method (aka M and M) as described by mathisfun.com.
	 */
	public double[] quartiles() {
		double[] set = Arrays.copyOf(dataSet, dataSet.length);
		Arrays.sort( set );
		double[] quartiles = new double[3];
		int middle = set.length/2;
		
		if ( set.length < 1 )
			throw new IllegalStateException("Empty data set");
		
		if ( set.length == 1 ) {
			quartiles[0] = quartiles[1] = quartiles[2] = set[0];
		} else {
			quartiles[0] = median( Arrays.copyOf( set, middle ) );
			quartiles[1] = median( set );
			quartiles[2] = median( 
					Arrays.copyOfRange( set,
					middle + (evenSetLength(set) ? 0 : 1),
					set.length ) );
		}
		
		return quartiles;
	}
	
	/**
	 * 
	 * @return Returns the variance of the dataset using the method described 
	 * in mathisfun.com; this is the method where the final step is to divide 
	 * by the length of the dataset ( not length  -  1).
	 */
	public double variance() {
		double sum = 0.;
		double mean = mean();
		for ( double datum : dataSet ) {
			double diff = datum - mean;
			sum += diff*diff;
		}
		double variance = 
				( dataSet.length > 1 ) ? sum / dataSet.length : Double.NaN;
		
		return variance;
	}
	
	/**
	 * 
	 * @return Returns the standard deviation of the dataset using the method 
	 * described in mathisfun.com.
	 */
	public double standardDeviation() {
		return Math.sqrt( variance() );
	}
	
	/**
	 * Calculate the distances between the median and all elements in the
	 * dataset. 
	 * @return return the largest one.
	 */
	public double maximumAbsoluteDeviation() {
		double[] set = Arrays.copyOf(dataSet, dataSet.length);
		double median = median();
		for ( int inx = 0; inx < set.length; inx++ )
			set[inx] = Math.abs( set[inx] - median );
		Arrays.sort( set );
		double max = set[set.length - 1];
		
		return max;
	}
	
	/**
	 * Create a new data set consisting of the distances between the median and 
	 * each element of the original data set. 
	 * @return Returns the median of the new dataset.
	 */
	public double medianAbsoluteDeviation() {
		double[] set = getSamples();
		double median = median();
		for ( int inx = 0; inx < set.length; inx++ )
			set[inx] = Math.abs( set[inx] - median );
		Arrays.sort(set);
		
		return medianFromSorted( set );
	}
	
	/**
	 * Create a new data set consisting of the distances between the median and 
	 * each element of the original data set.
	 * @return Returns the mean of the new dataset.
	 */
	public double meanAbsoluteDeviation() {
		double median = median();
		double sum = 0.;
		for ( int inx = 0; inx < dataSet.length; inx++ )
			sum += Math.abs( dataSet[inx] - median );
		
		return sum / dataSet.length;
	}
	
	/**
	 * @return Returns a copy of the dataset that it stores internally.
	 *	See note about consistent results in the introduction.
	 */
	public double[] getSamples() {
		return Arrays.copyOf( dataSet, dataSet.length );
	}
	
	private static boolean evenSetLength( double[] set ) {
		return set.length % 2 == 0;
	}
	
	private double median( double[] set ) {
		double[] workingSet = Arrays.copyOf(set, set.length);
		Arrays.sort( workingSet );
		return medianFromSorted( workingSet );
	}
	
	private double medianFromSorted( double[] set ) {
		double median;
		
		if ( set.length == 0 )
			throw new IllegalStateException("Empty data set");

		if ( evenSetLength(set) )
			median = average( set[set.length/2], set[set.length/2-1] );
		else
			median = set[set.length/2];
		
		return median;
	}
	
	private double average( double...nums ) {
		double sum = 0.;
		for ( double datum : nums ) {
			sum += datum;
		}
		double average = sum / nums.length;
		
		return average;
	}
	
}
