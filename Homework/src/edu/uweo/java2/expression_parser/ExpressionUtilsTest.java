package edu.uweo.java2.expression_parser;

import java.util.ArrayList;
import java.util.List;

public class ExpressionUtilsTest {
	ExpressionUtils expressionUtils;
	boolean expectedIsHexChar,
			expectedIsHexValue;
	int		expectedFindLastHexDigit,
			positionToTest;
	
//	expression, positionToTest, expectedIsHexChar, expectedIsHexValue, expectedFindLastHexDigit 
	private static Object[][] utilsData = {
		{ "5+5", 0, true, false, -1 },
		{ "0xf + 5", 2, true, false, -1 },
		{ "0xF + 5", 0, true, true, 2 },
	};
	
	private static List<ExpressionUtilsTest> euTestList = new ArrayList<>(utilsData.length);
	static {
		for (Object[] oa : utilsData)
			euTestList.add(new ExpressionUtilsTest(
					(String)oa[0],
					(int)(Integer)oa[1],
					(boolean)(Boolean)oa[2],
					(boolean)(Boolean)oa[3],
					(int)(Integer)oa[4]));
	}
	
	public static void main( String[] args ) {
		for (ExpressionUtilsTest test : euTestList)
			test.testAll();
	}
	
	private ExpressionUtilsTest() {}
	
	public ExpressionUtilsTest( String expressionToTest,
			int positionToTest,
			boolean expectedIsHexChar,
			boolean expectedIsHexValue,
			int expectedFindLastHexDigit ) {
		this();
		if (expressionToTest == null)
			throw new IllegalArgumentException("Test driver must be initialized with an ExpressionUtils instance");
		
		this.expressionUtils = new ExpressionUtils(expressionToTest);
		this.expectedIsHexChar = expectedIsHexChar;
		this.expectedIsHexValue = expectedIsHexValue;
		this.expectedFindLastHexDigit = expectedFindLastHexDigit;
		this.positionToTest = positionToTest;
	}
	
	public void testIsHexChar() {
		boolean actualIsHexChar = ExpressionUtils.isHexChar( expressionUtils.getCharacterExpression()[positionToTest] );
		if ( actualIsHexChar == expectedIsHexChar )
			System.out.println(passHeader() + "isHexChar");
		else {
			System.err.format(failHeader() + "isHexChar: expected '%b'; actual '%b'%n",
					expectedIsHexChar,
					actualIsHexChar);
		}
	}
	
	public void testIsHexValue() {
		boolean actualIsHexValue = expressionUtils.isHexValue( positionToTest );
		if ( actualIsHexValue == expectedIsHexValue )
			System.out.println(passHeader() + "isHexValue");
		else {
			System.err.format(failHeader() + "isHexValue: expected '%b'; actual '%b'%n",
					expectedIsHexValue,
					actualIsHexValue);
		}
	}
	
	public void testFindLastHexDigit() {
		int actualFindLastHexDigit = expressionUtils.findLastHexDigit( positionToTest );
		if ( actualFindLastHexDigit == expectedFindLastHexDigit )
			System.out.println(passHeader() + "findLastHexDigit");
		else {
			System.err.format(failHeader() + "findLastHexDigit: expected '%d'; actual '%d'%n",
					expectedFindLastHexDigit,
					actualFindLastHexDigit);
		}
	}
	
	public void testAll() {
//		added sleep because for some reason the tests were printing out of order
		long sleepTime = 5;
		try {
			testIsHexChar();
			Thread.sleep(sleepTime);
			testIsHexValue();
			Thread.sleep(sleepTime);
			testFindLastHexDigit();
			Thread.sleep(sleepTime);
			System.out.println();
		} catch ( InterruptedException e ) {
			System.err.println("Thread interrupted");
			System.exit(1);
		}
	}
	
	private String failHeader() {
		return "FAIL on '" + expressionUtils.getOriginalExpression() + "': ";
	}
	
	private String passHeader() {
		return "PASS on '" + expressionUtils.getOriginalExpression() + "': ";
	}
	
}
