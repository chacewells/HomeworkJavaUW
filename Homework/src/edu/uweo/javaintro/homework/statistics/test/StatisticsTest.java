package edu.uweo.javaintro.homework.statistics.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.uweo.javaintro.homework.statistics.Statistics;

public class StatisticsTest {
	
	@Test
	public void testMeanSetLengthTwo() {
		double[] data = {0,1};
		double expectedMean = .5;
		Statistics stats = new Statistics(data);
		assertEquals(expectedMean, stats.mean(), .0001);
	}
	
	@Test
	public void testMeanSetLengthThree() {
		double[] data = {0,1,2};
		double expectedMean = 1;
		Statistics stats = new Statistics(data);
		assertEquals(expectedMean, stats.mean(), .0001);
	}
	
	@Test
	public void testFirstQuartileSetLengthOne() {
		double[] data = {1};
		double expectedFirstQuartile = 1.;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[0], .0001);
	}
	
	@Test
	public void testSecondQuartileSetLengthOne() {
		double[] data = {1};
		double expectedFirstQuartile = 1.;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[1], .0001);
	}
	
	@Test
	public void testThirdQuartileSetLengthOne() {
		double[] data = {1};
		double expectedFirstQuartile = 1.;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[2], .0001);
	}
	
	@Test
	public void testFirstQuartileSetLengthTwo() {
		double[] data = {0,2};
		double expectedFirstQuartile = 0.;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[0], .0001);
	}
	
	@Test
	public void testSecondQuartileSetLengthTwo() {
		double[] data = {0,2};
		double expectedFirstQuartile = 1;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[1], .0001);
	}
	
	@Test
	public void testThirdQuartileSetLengthThree() {
		double[] data = {0,2};
		double expectedFirstQuartile = 2.;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[2], .0001);
	}
	
	@Test
	public void testFirstQuartileSetLengthFour() {
		double[] data = {1,2,3,4};
		double expectedFirstQuartile = 1.5;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[0], .0001);
	}
	
	@Test
	public void testSecondQuartileSetLengthFour() {
		double[] data = {1,2,3,4};
		double expectedFirstQuartile = 2.5;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[1], .0001);
	}
	
	@Test
	public void testThirdQuartileSetLengthFour() {
		double[] data = {1,2,3,4};
		double expectedFirstQuartile = 3.5;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[2], .0001);
	}
	
	@Test
	public void testFirstQuartileSetLengthFive() {
		double[] data = {1,2,3,4,5};
		double expectedFirstQuartile = 1.5;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[0], .0001);
	}
	
	@Test
	public void testSecondQuartileSetLengthFive() {
		double[] data = {1,2,3,4,5};
		double expectedFirstQuartile = 3;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[1], .0001);
	}
	
	@Test
	public void testThirdQuartileSetLengthFive() {
		double[] data = {1,2,3,4,5};
		double expectedFirstQuartile = 4.5;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[2], .0001);
	}
	
	@Test
	public void testFirstQuartileSetLengthSix() {
		double[] data = {1,2,3,4,5,6};
		double expectedFirstQuartile = 2;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[0], .0001);
	}
	
	@Test
	public void testSecondQuartileSetLengthSix() {
		double[] data = {1,2,3,4,5,6};
		double expectedFirstQuartile = 3.5;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[1], .0001);
	}
	
	@Test
	public void testThirdQuartileSetLengthSix() {
		double[] data = {1,2,3,4,5,6};
		double expectedFirstQuartile = 5;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[2], .0001);
	}
	
	@Test
	public void testFirstQuartileSetLengthSeven() {
		double[] data = {1,2,3,4,5,6,7};
		double expectedFirstQuartile = 2;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[0], .0001);
	}
	
	@Test
	public void testSecondQuartileSetLengthSeven() {
		double[] data = {1,2,3,4,5,6,7};
		double expectedFirstQuartile = 4;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[1], .0001);
	}
	
	@Test
	public void testThirdQuartileSetLengthSeven() {
		double[] data = {1,2,3,4,5,6,7};
		double expectedFirstQuartile = 6;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[2], .0001);
	}
	
	@Test
	public void testFirstQuartileSetLengthEight() {
		double[] data = {1,2,3,4,5,6,7,8};
		double expectedFirstQuartile = 2.5;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[0], .0001);
	}
	
	@Test
	public void testSecondQuartileSetLengthEight() {
		double[] data = {1,2,3,4,5,6,7,8};
		double expectedFirstQuartile = 4.5;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[1], .0001);
	}
	
	@Test
	public void testThirdQuartileSetLengthEight() {
		double[] data = {1,2,3,4,5,6,7,8};
		double expectedFirstQuartile = 6.5;
		Statistics stats = new Statistics(data);
		assertEquals(expectedFirstQuartile, stats.quartiles()[2], .0001);
	}

}
