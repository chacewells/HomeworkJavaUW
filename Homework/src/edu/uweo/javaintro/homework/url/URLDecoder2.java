package edu.uweo.javaintro.homework.url;

public class URLDecoder2 extends URLManager {
	URLQueryDecoder queryDecoder;
	String toDecode;
	private static final String		
		COLON_SLASH_SLASH	= "://",
		COLON				= ":",
		SLASH				= "/",
		COLON_OR_SLASH		= "[" + COLON + SLASH + "]",
		QMARK				= "?",
		HASH				= "#",
		QMARK_OR_HASH		= "[" + QMARK + HASH + "]",
		PROTOCOL			= "[A-Za-z0-9]+" + COLON_SLASH_SLASH,
		DOMAIN_CHARS		= "[A-Za-z0-9\\-]";
	
	public URLDecoder2( String toDecode ) {
		this.toDecode = toDecode;
	}
	
	public void decode() {
		
	}

	@Override
	public String getQueryString() {
		return null;
	}

}
