package edu.uweo.javaintro.homework.url.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.uweo.javaintro.homework.url.URLQueryEncoder;

public class URLQueryEncoderTest {

	@Test
	public void encodeReserved() {
		String toEncode = "/?#[]@!$&()+,;=";
		String expected = "a=%2F%3F%23%5B%5D%40%21%24%26%28%29%2B%2C%3B%3D";
		URLQueryEncoder encoder = new URLQueryEncoder();
		encoder.addQueryPair( "a", toEncode );
		assertEquals( expected, encoder.encode() );
	}
	
	@Test
	public void encodeLessThan0x20() {
		String toEncode = "" + '\u001F';
		String expected = "a=%1F";
		URLQueryEncoder encoder = new URLQueryEncoder();
		encoder.addQueryPair( "a", toEncode );
		assertEquals( expected, encoder.encode() );
	}
	
	@Test
	public void encodeGreaterThan0x7F() {
		String toEncode = "" + '\u0080';
		String expected = "a=%80";
		URLQueryEncoder encoder = new URLQueryEncoder();
		encoder.addQueryPair( "a", toEncode );
		assertEquals( expected, encoder.encode() );
	}
	
	@Test
	public void encodeLessThan0x10() {
		String toEncode = "\u000F";
		String expected = "a=%0F";
		URLQueryEncoder encoder = new URLQueryEncoder();
		encoder.addQueryPair( "a", toEncode );
		assertEquals( expected, encoder.encode() );
	}
	
}
