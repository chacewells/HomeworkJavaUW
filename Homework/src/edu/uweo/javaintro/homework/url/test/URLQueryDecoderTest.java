package edu.uweo.javaintro.homework.url.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import edu.uweo.javaintro.homework.url.NameValuePair;
import edu.uweo.javaintro.homework.url.URLQueryDecoder;

public class URLQueryDecoderTest {

	@Test
	public void decode() {
		URLQueryDecoder d =
				new URLQueryDecoder( 
						"quote=Vini%2C%20vidi%2C%20vici%21"
						+ "&attributedTo=Caesar"
						+ "&q=marilyn+monroe"
						+ "&search=i+don't+want+to+die+young%2C+i'd+much+rather+live+on+a+boat+%26+breathe!"
					);
		
		Map<String,String> map = new HashMap<String,String>();
		map.put("quote", "Vini, vidi, vici!");
		map.put("attributedTo", "Caesar");
		map.put("q", "marilyn monroe");
		map.put("search", "i don't want to die young, i'd much rather live on a boat & breathe!");
		
		assertEquals( map, d.decode() );
	}
	
	@Test
	public void getQueryMap() {
		URLQueryDecoder d = new URLQueryDecoder( "quote=Vini%2C%20vidi%2C%20vici%21" );
		Map<String,String> testMap = new HashMap<>();
		
		assertEquals(testMap, d.getQueryMap());
		
		d.decode();
		
		assertNotEquals(testMap, d.getQueryMap());
	}
	
	@Test
	public void getQueryList() {
		List<NameValuePair> l = new ArrayList<>();
		l.add( new NameValuePair( "quote", "Vini, vidi, vici!" ) );
		
		URLQueryDecoder decoder =
				new URLQueryDecoder( "quote=Vini%2C%20vidi%2C%20vici%21" );
		
		assertEquals( l, decoder.getQueryList() );
	}
	
	@Test( expected = IllegalArgumentException.class )
	public void decodeIllegalArgumentException() {

		URLQueryDecoder d =
				new URLQueryDecoder( 
						"quote=Vini%2C%2Gvidi%2C%20vici%21"
						+ "&attributedTo=Caesar"
						+ "&q=marilyn+monroe"
						+ "&search=i+don't+want+to+die+young%2C+i'd+much+rather+live+on+a+boat+%26+breathe!"
					);
		d.decode();
	}
	
}
