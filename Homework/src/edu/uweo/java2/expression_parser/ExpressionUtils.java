package edu.uweo.java2.expression_parser;

import java.util.Arrays;
import java.util.Stack;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Provides an abstraction layer for analyzing a simple expression string.
 * @author Aaron Wells
 */
class ExpressionUtils {
	private static final String DECIMAL_VAL_REGEX = "\\d*\\.?\\d+";
	private final String	oExpression,
							sExpression;
	private final char[]	cExpression;
	
	/**
	 * Initializes a new receiver with the given expression string
	 * @author Aaron Wells
	 * @param expression the expression to be analyzed
	 */
	public ExpressionUtils(String expression) {
		oExpression = expression;
		sExpression = strip( oExpression );
		cExpression = sExpression.toCharArray();
	}
	
//	CHAR TESTS
	
	/**
	 * Tests whether a character represents a hexadecimal digit
	 * @author Aaron Wells
	 * @param test the character to be tested
	 * @return true if 'test' is a hexadecimal digit; false otherwise
	 */
	public static boolean isHexChar( char test ) {
		return (""+test).matches("[0-9A-Fa-f]");
	}
	
	/**
	 * Tests whether the character is a valid symbol
	 * @author Aaron Wells
	 * @param test the character to test
	 * @return true if the character is a digit, an upper- or lowercase letter,
	 * or an underscore (_); false otherwise
	 */
	public static boolean isSymbolChar( char test ) {
		return (""+test).matches("\\w");
	}

	/**
	 * Tests whether the character represents a binary digit
	 * @author Aaron Wells
	 * @param test the character to test
	 * @return true if the character is either '0' or '1'; false otherwise
	 */
	public static boolean isBinaryChar( char test ) {
		return (""+test).matches("[01]");
	}
	
//	VALUE TESTS
	
//	Abstracts algorithm used to test value type by substring
	private boolean isValueByRegex( int start, String regex ) {
		boolean result = false;
		String stRegex = regex + ".*?";
		
		String substr = sExpression.substring(start);
		result = substr.matches(stRegex);
		
		return result;
	}
	
	/**
	 * Tests whether the substring at the given index is a decimal
	 * representation of an integer
	 * @author Aaron Wells
	 * @param next the starting index of the substring to be analyzed
	 * @return <b>true</b> if the substring starts with a digit or the substring
	 * starts with a decimal point (.) followed by a digit; <B>false</B> otherwise
	 */
	public boolean isDecimalValue( int next ) {
		return isValueByRegex(next, DECIMAL_VAL_REGEX);
	}
	
	/**
	 * Tests whether the substring at the given index is a binary representation
	 * of an integer
	 * @author Aaron Wells
	 * @param next the starting index of the substring to be analyzed
	 * @return <B>true</B> if the substring begins with '0b' or '0B', and the next digit
	 * is a binary digit ('0' or '1'); <B>false</B> otherwise
	 */
	public boolean isBinaryValue( int next ) {
		return isValueByRegex(next, "0[Bb][01]");
	}
	
	/**
	 * Tests whether the substring at the given index is a hexadecimal
	 * representation of an integer
	 * @author Aaron Wells
	 * @param next the starting index of the substring to be analyzed
	 * @return <B>true</B> if the substring begins with '0x' or '0X', and the next digit
	 * is a hexadecimal digit (0-9A-Fa-f); <B>false</B> otherwise
	 */
	public boolean isHexValue( int next ) {
		return isValueByRegex(next, "0[Xx][0-9A-Fa-f]");
	}
	
	/**
	 * Tests whether the substring at the given index is a valid symbol
	 * @author Aaron Wells
	 * @param next the starting index of the substring to be analyzed
	 * @return <B>true</B> if the substring begins with an upper- or lowercase
	 * character or an underscore; <B>false</B> otherwise
	 */
	public boolean isSymbolValue( int next ) {
		return isValueByRegex(next, "[A-Za-z_]");
	}
	
	/**
	 * Tests whether the substring at the given index is a valid function
	 * @param next the starting index of the substring to be analyzed
	 * @return <B>true</B> if the substring begins with A-Z, a-z, or underscore
	 * (_), followed by a string of zero or more symbol characters (A-Za-z0-9_);
	 *  <B>false</B> otherwise
	 */
	public boolean isFunctionValue( int next ) {
		return isValueByRegex(next, "[A-Za-z_]\\w*\\(");
	}
	
//	INDEX FINDERS
	
//	Abstracts algorithm used to find the last character in a substring
	private int findLastCharOfType( int from, 
			int offset,
			Predicate<Integer> failCheck, 
			String failMessage,
			Predicate<Character> moreChars ) {
		int result = from + offset;
		char nextChar = cExpression[result];
		
		if ( !failCheck.test(from) )
			throw new IllegalArgumentException(failMessage);
		
		try {
			while ( moreChars.test(nextChar) )
				nextChar = cExpression[++result];
		} catch ( ArrayIndexOutOfBoundsException e ) {}
		result -= 1;
		
		return result;
	}

	private int findLastCharOfType( int from, 
			Predicate<Integer> failCheck,
			String failMessage,
			Predicate<Character> moreChars ) {
		return findLastCharOfType(from, 0, failCheck, failMessage, moreChars);
	}

	/**
	 * Finds the closing parenthesis from the position of its
	 * corresponding opening parenthesis
	 * @author Aaron Wells
	 * @param from the index of the opening parenthesis
	 * @return the index of the closing parenthesis if it exists; the
	 * expression length if the closing parenthesis does not exist
	 * @throws IllegalArgumentException if character at <B>from</B> is not '('
	 */	
	public int findClosingParenthesis( int from ) {
		int pos = from;
		Stack<Character> parens = new Stack<>();
		
		if ( cExpression[pos] != '(' )
			throw new IllegalArgumentException("The index given does not mark a"
					+ " '(' character");
		
		while ( pos < cExpression.length ) {
			char c = cExpression[pos];
			if ( c == '(' ) parens.push(c);
			if ( c == ')' ) parens.pop();
			if ( parens.empty() ) break;
			pos++;
		}
		
		return pos;
	}
	
	/**
	 * Finds the index of the last symbol character in the substring beginning 
	 * with the given index
	 * @author Aaron Wells
	 * @param from the index where the substring begins
	 * @return the index of the last symbol character in the substring
	 * @throws IllegalArgumentException if substring at <B>from</B> is not a
	 * valid symbol value
	 */
	public int findLastSymbolChar( int from ) {
		return findLastCharOfType( from,
				this::isSymbolValue,
				"The substring at the given index is not a valid symbol value",
				ExpressionUtils::isSymbolChar);
	}

	/**
	 * Finds the index of the last decimal digit in the substring beginning 
	 * with the given index
	 * @author Aaron Wells
	 * @param from the index where the substring begins
	 * @return the index of the last decimal digit in the substring
	 * @throws IllegalArgumentException if substring at <B>from</B> is not a
	 * valid decimal value
	 */
	public int findLastDecimalDigit( int from ) {
		if ( !isDecimalValue(from) )
			throw new IllegalArgumentException("The substring at the given"
					+ " index is not a valid decimal value");
		
		Pattern p = Pattern.compile(DECIMAL_VAL_REGEX);
		Matcher m = p.matcher( sExpression );
		m.find(from);
		
		return m.end() - 1;
	}

	/**
	 * Finds the last digit in the hexadecimal substring beginning with the
	 * given index
	 * @author Aaron Wells
	 * @param from the index where the substring begins
	 * @return the index of the last hexadecimal digit in the substring
	 * @throws IllegalArgumentException if substring at <B>from</B> is not a
	 * valid hexadecimal value
	 */
	public int findLastHexDigit(int from) {
		return findLastCharOfType(from,
				2,
				this::isHexValue, 
				"The substring at the given index is not a valid hexadecimal value", 
				ExpressionUtils::isHexChar);
	}

	/**
	 * Finds the position of the last digit in the binary substring beginning
	 * with the given index
	 * @author Aaron Wells
	 * @param from the index where the substring begins
	 * @return the index of the last binary digit in the substring
	 * @throws IllegalArgumentException if substring at <B>from</B> is not a
	 * valid binary value
	 */
	public int findLastBinaryDigit( int from ) {
		return findLastCharOfType(from, 2, this::isBinaryValue, 
				"The substring at the given index is not a valid binary value", 
				ExpressionUtils::isBinaryChar);
	}
	
	/**
	 * Returns this receiver's original string expression
	 * @author Aaron Wells
	 * @return the original string passed into the one argument String
	 * constructor
	 */
	public String getOriginalExpression() {
		return oExpression;
	}
	
	/**
	 * Returns this receiver's stripped string expression
	 * @author Aaron Wells
	 * @return the original string expression, less all whitespace characters
	 */
	public String getStringExpression() {
		return sExpression;
	}
	
	/**
	 * Returns the characters from this receiver's stripped string expression
	 * @author Aaron Wells
	 * @return a character array consisting of the characters in the stripped
	 * string expression in the same sequential order
	 * @throws NullPointerException if this receiver's character expression
	 * is null
	 */
	public char[] getCharacterExpression() {
		return cExpression.clone(); // Arrays.copyOf(cExpression, cExpression.length);
	}
	
	/**
	 * Returns a copy of the input String less whitespace.
	 * @param str the string to strip
	 * @return the string less whitespace
	 */
	public static String strip( String str ) {
		return str.replaceAll("\\s+", "");
	}
	
	@Override
	public String toString() {
		return "ExpressionUtils [originalExpression=" + oExpression
				+ ", strippedExpression=" + sExpression
				+ ", characterExpression="
				+ Arrays.toString(cExpression) + "]";
	}

}
