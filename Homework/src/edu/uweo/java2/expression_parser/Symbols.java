package edu.uweo.java2.expression_parser;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides utilities for mapping case insensitive keys to corresponding
 * double values. This class cannot be instantiated.
 * @author Aaron Wells
 * 
 */
class Symbols {
	
	private static final Map<HashKey, Double> predefinedSymbolMap_;
	static {
		Map<HashKey,Double> m = new HashMap<>(2);
		m.put(new HashKey("PI"), Math.PI);
		m.put(new HashKey("E"), Math.E);
		predefinedSymbolMap_ = Collections.unmodifiableMap(m);
	}
	private static Map<HashKey, Double> userSymbolMap_ = new HashMap<>();
	
	/**
	 * Maps a user-defined symbol to a numeric value.
	 * @author Aaron Wells
	 * @param name The symbol name.
	 * @param value The Numeric value retrieved.
	 */
	public static void addSymbol( String name, double value ) {
		userSymbolMap_.put( new HashKey(name), value);
	}
	
	/**
	 * Retrieves a symbol by its corresponding key name.
	 * @param name The symbol name.
	 * @return The symbol value.
	 * @throws IllegalArgumentException If the symbol name cannot be found.
	 */
	public static double getSymbol( String name ) {
		Double value = null;
		HashKey key = new HashKey(name);
		value = userSymbolMap_.get( key );
		if ( value == null )
			value = predefinedSymbolMap_.get( key );
		if ( value == null )
			throw new IllegalArgumentException("The symbol value was not"
					+ " found");
		
		return value.doubleValue();
	}
	
	private Symbols(){}
	
	private static class HashKey {
		int hashCode;
		String key;
		
		public HashKey( String key ) {
			this.key = key;
			hashCode();
		}

		@Override
		public int hashCode() {
			hashCode = key.toLowerCase().hashCode();
			return hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if ( !(obj instanceof HashKey) ) return false;
			return key.equalsIgnoreCase( ((HashKey)obj).key );
		}
	}
	
}
