package edu.uweo.javaintro.homework.statistics.test_driver;

import java.util.Arrays;

import edu.uweo.javaintro.homework.statistics.Statistics;

public class TestCaseStatistics implements TestCase {
	
	private double[] dataSet_;
	Statistics stats_;
	
	private double	expectedMean_,
					expectedMedian_,
					expectedFirstQuartile_,
					expectedSecondQuartile_,
					expectedThirdQuartile_,
					expectedMinimum_,
					expectedMaximum_,
					expectedRange_,
					expectedVariance_,
					expectedStandardDeviation_,
					expectedMaximumAbsoluteDeviation_,
					expectedMedianAbsoluteDeviation_,
					expectedMeanAbsoluteDeviation_;
	
	private TestCaseStatistics( TestCaseBuilder tcb ) {
		dataSet_ = tcb.getDataSet();
		
		expectedMean_ = tcb.getExpectedMean();
		expectedMedian_ = tcb.getExpectedMedian();
		expectedFirstQuartile_ = tcb.getExpectedFirstQuartile();
		expectedSecondQuartile_ = tcb.getExpectedSecondQuartile();
		expectedThirdQuartile_ = tcb.getExpectedThirdQuartile();
		expectedMinimum_ = tcb.getExpectedMinimum();
		expectedMaximum_ = tcb.getExpectedMaximum();
		expectedRange_ = tcb.getExpectedRange();
		expectedVariance_ = tcb.getExpectedVariance();
		expectedStandardDeviation_ = tcb.getExpectedStandardDeviation();
		expectedMaximumAbsoluteDeviation_ = tcb.getExpectedMaximumAbsoluteDeviation();
		expectedMedianAbsoluteDeviation_ = tcb.getExpectedMedianAbsoluteDeviation();
		expectedMeanAbsoluteDeviation_ = tcb.getExpectedMeanAbsoluteDeviation();
	}
	
	private Statistics getStats() {
		if (stats_ == null)
			stats_ = new Statistics( dataSet_ );
		return stats_;
	}
	
	private void printHeader() {
		System.out.print("Testing data set: ");
		for ( double datum : dataSet_ )
			System.out.format( "%.2f ", datum );
		System.out.println();
		for ( int i : new int[40] )
			System.out.print('*');
		System.out.println();
	}

	@Override
	public void test() {
		printHeader();
		
		testMean();
		testMedian();
		testFirstQuartile();
		testSecondQuartile();
		testThirdQuartile();
		testMinimum();
		testMaximum();
		testRange();
		testVariance();
		testStandardDeviation();
		testMaximumAbsoluteDeviation();
		testMedianAbsoluteDeviation();
		testMeanAbsoluteDeviation();
		System.out.println();
	}
	public void testMean() {
		testParam("mean", expectedMean_, getStats().mean(), 0.0001);
	}
	public void testMedian() {
		testParam("median", expectedMedian_, getStats().median(), 0.0001);
	}
	public void testFirstQuartile() {
		double[] quartiles = getStats().quartiles();
		if ( quartiles == null )
			throw new Error("No quartile data found");
		testParam("first quartile", expectedFirstQuartile_, quartiles[0], 0.0001);
	}
	public void testSecondQuartile() {
		double[] quartiles = getStats().quartiles();
		if ( quartiles == null )
			throw new Error("No quartile data found");
		testParam("second quartile", expectedSecondQuartile_, quartiles[1], 0.0001);
	}
	public void testThirdQuartile() {
		double[] quartiles = getStats().quartiles();
		if ( quartiles == null )
			throw new Error("No quartile data found");
		testParam("third quartile", expectedThirdQuartile_, quartiles[2], 0.0001);
	}
	public void testMinimum() {
		testParam("minimum", expectedMinimum_, getStats().minimum(), 0.0001);
	}
	public void testMaximum() {
		testParam("maximum", expectedMaximum_, getStats().maximum(), 0.0001);
	}
	public void testRange() {
		testParam("range", expectedRange_, getStats().range(), 0.0001);
	}
	public void testVariance() {
		testParam("variance", expectedVariance_, getStats().variance(), 0.0001);
	}
	public void testStandardDeviation() {
		testParam("standard deviation", expectedStandardDeviation_, getStats().standardDeviation(), 0.0001);
	}
	public void testMaximumAbsoluteDeviation() {
		testParam("maximum absolute deviation", expectedMaximumAbsoluteDeviation_, getStats().maximumAbsoluteDeviation(), 0.0001);
	}
	public void testMedianAbsoluteDeviation() {
		testParam("median absolute deviation", expectedMedianAbsoluteDeviation_, getStats().medianAbsoluteDeviation(), 0.0001);
	}
	public void testMeanAbsoluteDeviation() {
		testParam("mean absolute deviation", expectedMeanAbsoluteDeviation_, getStats().meanAbsoluteDeviation(), 0.0001);
	}
	private static boolean isWithinEpsilon( double actual, double expected, double epsilon ) {
		return ( actual < (expected + epsilon) ) &&
				( actual > (expected - epsilon) );
	}
	private static void pass( String param ) {
		System.out.println("PASS: " + param);
	}
	private static void testParam( String param, double expected, double actual, double epsilon ) {
		if ( !isWithinEpsilon(actual, expected, epsilon) )
			throw new Error("FAIL "+param+": expected["+expected+"] " +
					"actual["+actual+"]");
		else
			pass(param);
	}
	
	public static class TestCaseBuilder {
		
		private double[] dataSet_;
		
		private double	expectedMean_,
						expectedMedian_,
						expectedFirstQuartile_,
						expectedSecondQuartile_,
						expectedThirdQuartile_,
						expectedMinimum_,
						expectedMaximum_,
						expectedRange_,
						expectedVariance_,
						expectedStandardDeviation_,
						expectedMaximumAbsoluteDeviation_,
						expectedMedianAbsoluteDeviation_,
						expectedMeanAbsoluteDeviation_;
		
		public TestCaseBuilder( double[] dataSet ) {
			dataSet_ = Arrays.copyOf(dataSet, dataSet.length);
		}
		public TestCaseStatistics build() {
			return new TestCaseStatistics(this);
		}
		public TestCaseBuilder mean( double expectedMean ) {
			expectedMean_ = expectedMean;
			return this;
		}
		public TestCaseBuilder median( double expectedMedian ) {
			expectedMedian_ = expectedMedian;
			return this;
		}
		public TestCaseBuilder firstQuartile( double expectedFirstQuartile ) {
			expectedFirstQuartile_ = expectedFirstQuartile;
			return this;
		}
		public TestCaseBuilder secondQuartile( double expectedSecondQuartile ) {
			expectedSecondQuartile_ = expectedSecondQuartile;
			return this;
		}
		public TestCaseBuilder thirdQuartile( double expectedThirdQuartile ) {
			expectedThirdQuartile_ = expectedThirdQuartile;
			return this;
		}
		public TestCaseBuilder minimum( double expectedMinimum ) {
			expectedMinimum_=expectedMinimum;
			return this;
		}
		public TestCaseBuilder maximum( double expectedMaximum ) {
			expectedMaximum_=expectedMaximum;
			return this;
		}
		public TestCaseBuilder range( double expectedRange ) {
			expectedRange_=expectedRange;
			return this;
		}
		public TestCaseBuilder variance( double expectedVariance ) {
			expectedVariance_=expectedVariance;
			return this;
		}
		public TestCaseBuilder standardDeviation( double expectedStandardDeviation ) {
			expectedStandardDeviation_=expectedStandardDeviation;
			return this;
		}
		public TestCaseBuilder maximumAbsoluteDeviation( double expectedMaximumAbsoluteDeviation ) {
			expectedMaximumAbsoluteDeviation_=expectedMaximumAbsoluteDeviation;
			return this;
		}
		public TestCaseBuilder medianAbsoluteDeviation( double expectedMedianAbsoluteDeviation ) {
			expectedMedianAbsoluteDeviation_=expectedMedianAbsoluteDeviation;
			return this;
		}
		public TestCaseBuilder meanAbsoluteDeviation( double expectedMeanAbsoluteDeviation ) {
			expectedMeanAbsoluteDeviation_=expectedMeanAbsoluteDeviation;
			return this;
		}
		public double[] getDataSet() {
			return Arrays.copyOf(dataSet_, dataSet_.length);
		}
		public double getExpectedMean() {
			return expectedMean_;
		}
		public double getExpectedMedian() {
			return expectedMedian_;
		}
		public double getExpectedFirstQuartile() {
			return expectedFirstQuartile_;
		}
		public double getExpectedSecondQuartile() {
			return expectedSecondQuartile_;
		}
		public double getExpectedThirdQuartile() {
			return expectedThirdQuartile_;
		}
		public double getExpectedMinimum() {
			return expectedMinimum_;
		}
		public double getExpectedMaximum() {
			return expectedMaximum_;
		}
		public double getExpectedRange() {
			return expectedRange_;
		}
		public double getExpectedVariance() {
			return expectedVariance_;
		}
		public double getExpectedStandardDeviation() {
			return expectedStandardDeviation_;
		}
		public double getExpectedMaximumAbsoluteDeviation() {
			return expectedMaximumAbsoluteDeviation_;
		}
		public double getExpectedMedianAbsoluteDeviation() {
			return expectedMedianAbsoluteDeviation_;
		}
		public double getExpectedMeanAbsoluteDeviation() {
			return expectedMeanAbsoluteDeviation_;
		}
	}
	
}
