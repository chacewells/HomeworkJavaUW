package edu.uweo.javaintro.homework.url.URLsPart1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class URLQueryEncoder {
	private static final String RESERVED_CHARS = "[\\Q/?#[]@!$&()+,;= %\\E]";
	private List<NameValuePair> queryPairs;
	
	public URLQueryEncoder( List<NameValuePair> queryPairs ) {
		setQueryPairs( queryPairs );
	}
	
	public URLQueryEncoder() {
		this( new ArrayList<NameValuePair>() );
	}
	
	public List<NameValuePair> getQueryPairs() {
		return new ArrayList<NameValuePair>( queryPairs );
	}
	
	public void setQueryPairs( List<NameValuePair> queryPairs ) {
		this.queryPairs = new ArrayList<NameValuePair>( queryPairs );
	}
	
	public void addQueryPair( NameValuePair pair ) {
		queryPairs.add( pair );
	}
	
	public void addQueryPair( String name, String value ) {
		queryPairs.add( new NameValuePair( name, value ) );
	}
	
	public String encode() {
		StringBuilder encoded = new StringBuilder();

		Iterator<NameValuePair> itr = queryPairs.iterator();
		
		while ( itr.hasNext() ) {
			NameValuePair pair = itr.next();
			encoded.append( encode( pair.getName() ) );
			encoded.append( '=' );
			encoded.append( encode( pair.getValue() ) );
			encoded.append( itr.hasNext() ? '&' : "" );
		}
		
		return encoded.toString();
	}
	
	private static String encode( String str ) {
		StringBuilder encoded = new StringBuilder();
		
		for ( char c : str.toCharArray() )
			encoded.append( encode(c) );
		
		return encoded.toString();
	}
	
	private static String encode( char c ) {
		String out = "";
		if ( needsEncoding(c) ) 
			out = ( c == ' ' ) ? "+" : String.format("%c%02x", '%', Integer.valueOf(c)).toUpperCase();
		else
			out = "" + c;
		
		return out;
	}
	
	private static boolean isReserved( char c ) {
		return ("" + c).matches(RESERVED_CHARS);
	}
	
	private static boolean needsEncoding( char c ) {
		boolean needsEncoding = false;
		
		boolean isReserved = isReserved(c);
		needsEncoding = needsEncoding || isReserved;
		
		boolean lessThanHex20 = c < 0x20;
		needsEncoding = needsEncoding || lessThanHex20;
		
		boolean greaterThanOrEqualToHex7F = c >= 0x7F;
		needsEncoding = needsEncoding || greaterThanOrEqualToHex7F;
		
		return needsEncoding;
	}
	
}
