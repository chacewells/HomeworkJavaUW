package edu.uweo.java2.expression_parser;

import java.util.function.Predicate;

public class ExpressionUtils2 {
	
	char[] cExpression;
	
	public int findLastCharOfType(
			int from,
			Predicate<Character> failCheck,
			String failMessage,
			Predicate<Character> moreChars) {
		int result = from;
		char nextChar = cExpression[result];
		
		if ( !failCheck.test(nextChar) )
			throw new IllegalArgumentException(failMessage);
		
		try {
			while ( moreChars.test(nextChar) )
				nextChar = cExpression[++result];
		} catch ( ArrayIndexOutOfBoundsException e ) {}
		result -= 1;
		
		return result;
	}
	
	public boolean dumb(int next) {
		boolean result = false;
		boolean nextIsDot = cExpression[next] == '.';
		int offset = nextIsDot ? 2 : 1;
		String regex = nextIsDot ? "\\.\\d" : "\\d";
		
		result = isValueByRegex(next, offset, regex);
		return result;
		
//		isFunctionValue()
//		int position = next;
//		char nextChar = cExpression[position];
//		
//		try {
//			if ( !isSymbolValue(next) )
//				throw new IllegalArgumentException();
//			while ( isSymbolChar(nextChar) )
//				nextChar = cExpression[++position];
//			if ( nextChar == '(' )
//				result = true;
//		} catch ( IllegalArgumentException e ) {}
		
//		isSymbolValue()
//		boolean result = false;
//		
//		try {
//			if ( (""+cExpression[next]).matches("[A-Za-z_]") )
//				result = true;
//		} catch ( ArrayIndexOutOfBoundsException e ) {}

//		isHexValue()
//		boolean result = false;
//		
//		try {
//			boolean startsWithZero = cExpression[next] == '0';
//			boolean followedByX = cExpression[next+1] == 'x' || cExpression[next+1] == 'X';
//			boolean followedByHexDigit = isHexChar( cExpression[next+2] );
//			
//			result = startsWithZero && followedByX && followedByHexDigit;
//		} catch ( ArrayIndexOutOfBoundsException e ) {} 
		
//		isDecimalValue()
//		boolean result = false;
//		
//		try {
//			if ( cExpression[next] == '.' ) {
//				if ( (""+cExpression[next+1]).matches("\\d") )
//					result = true;
//			} else if ( (""+cExpression[next]).matches("\\d") )
//				result = true;
//		} catch ( ArrayIndexOutOfBoundsException e ) {}
//		
//		return result;
	}
	
	boolean isValueByRegex(int a, int b, String r) {return false;}
}
