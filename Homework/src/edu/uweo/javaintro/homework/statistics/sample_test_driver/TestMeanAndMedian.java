package edu.uweo.javaintro.homework.statistics.sample_test_driver;

public class TestMeanAndMedian
{
    // Put an array of test case objects here, for example:
    private static TestCaseMM[]    testCases_  = 
    {
        new TestCaseMM( new double[] { 5, 8, 4, 4, 6, 3, 8 },
                        5.42857142857, 5 
        ),
        new TestCaseMM( new double[] { 600, 470, 170, 430, 300 },
                        394, 430 
        ),
        // put more test cases here.
    };
    
    public static void main( String[] args )
    {
        for ( int inx = 0 ; inx < testCases_.length ; ++inx )
        {
            System.out.println( "Test case " + inx );
            testCases_[inx].test();
        }
    }
}
