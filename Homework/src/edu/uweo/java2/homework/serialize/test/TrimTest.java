package edu.uweo.java2.homework.serialize.test;

import static junit.framework.TestCase.assertEquals;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

import org.junit.Test;

import edu.uweo.java2.homework.serialize.SerializableObject;
import edu.uweo.java2.homework.serialize.Trim;

public class TrimTest {
	
	@SuppressWarnings("unchecked")
	@Test
	public void trimTest() throws ClassNotFoundException {
		Trim t = new Trim(Trim.FILE_IN, Trim.FILE_OUT);
		t.read();
		List<SerializableObject> expected = t.trimmedList();
		t.write();
		
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(Trim.FILE_OUT));
			List<SerializableObject> trimmed = (List<SerializableObject>) in.readObject();
			in.close();
			assertEquals(expected, trimmed);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}
