package edu.uweo.java2.expression_parser;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SymbolsJUnit {

	@Test
	public void testUserDefined() {
		String name = "KeY";
		Symbols.addSymbol(name, 20);
		double expected = 20;
		assertEquals((Double)expected, (Double)Symbols.getSymbol("key"));
	}
	
	@Test
	public void predefined() {
		String pi = "pi";
		double expected = Math.PI;
		assertEquals((Double)expected, (Double)Symbols.getSymbol(pi));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void getSymbolException() {
		String symbol = "symbol";
		try {
			Symbols.getSymbol(symbol);
		} catch ( Exception e ) {
			e.printStackTrace();
			throw e;
		}
	}

}
