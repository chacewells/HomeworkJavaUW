package edu.uweo.java2.expression_parser;

import java.util.Arrays;

/**
 * Provides an abstraction layer for analyzing a simple expression string.
 * @author Aaron Wells
 */
public class ExpressionUtilsBefore {
	private final String	oExpression,
							sExpression;
	private final char[]	cExpression;
	
	/**
	 * Initializes a new receiver with the given expression string
	 * @author Aaron Wells
	 * @param expression the expression to be analyzed
	 */
	public ExpressionUtilsBefore(String expression) {
		oExpression = expression;
		sExpression = strip( oExpression );
		cExpression = sExpression.toCharArray();
	}
	
//	CHAR TESTS
	
	/**
	 * Tests whether a character represents a hexadecimal digit
	 * @author Aaron Wells
	 * @param test the character to be tested
	 * @return true if 'test' is a hexadecimal digit; false otherwise
	 */
	public static boolean isHexChar( char test ) {
		boolean result = false;
		
		try {
			Integer.parseInt("" + test, 16);
			result = true;
		} catch ( NumberFormatException e ) {}
		
		return result;
	}
	
	/**
	 * Tests whether the character is a valid symbol
	 * @author Aaron Wells
	 * @param test the character to test
	 * @return true if the character is a digit, an upper- or lowercase letter,
	 * or an underscore (_); false otherwise
	 */
	public static boolean isSymbolChar(char test) {
		return (""+test).matches("\\w");
	}

	/**
	 * Tests whether the character represents a binary digit
	 * @author Aaron Wells
	 * @param test the character to test
	 * @return true if the character is either '0' or '1'; false otherwise
	 */
	public static boolean isBinaryChar(char test) {
		return test == '0' || test == '1';
	}
	
//	VALUE TESTS
	
	/**
	 * Tests whether the substring at the given index is a decimal
	 * representation of an integer
	 * @author Aaron Wells
	 * @param next the starting index of the substring to be analyzed
	 * @return <>true</b> if the substring starts with a digit or the substring
	 * starts with a decimal point (.) followed by a digit; <B>false</B> otherwise
	 */
	public boolean isDecimalValue( int next ) {
		boolean result = false;
		
		try {
			if ( cExpression[next] == '.' ) {
				if ( (""+cExpression[next+1]).matches("\\d") )
					result = true;
			} else if ( (""+cExpression[next]).matches("\\d") )
				result = true;
		} catch ( ArrayIndexOutOfBoundsException e ) {}
		
		return result;
	}
	
	/**
	 * Tests whether the substring at the given index is a binary representation
	 * of an integer
	 * @author Aaron Wells
	 * @param next the starting index of the substring to be analyzed
	 * @return <B>true</B> if the substring begins with '0b' or '0B', and the next digit
	 * is a binary digit ('0' or '1'); <B>false</B> otherwise
	 */
	public boolean isBinaryValue( int next ) {
		boolean result = false;

		try {
			boolean startsWithZero = cExpression[next] == '0';
			boolean followedByB = cExpression[next+1] == 'b' || cExpression[next+1] == 'B';
			boolean followedByBinaryDigit = isBinaryChar( cExpression[next+2] );
			
			result = startsWithZero && followedByB && followedByBinaryDigit;
		} catch ( ArrayIndexOutOfBoundsException e ) {}
		
		return result;
	}
	
	/**
	 * Tests whether the substring at the given index is a hexadecimal
	 * representation of an integer
	 * @author Aaron Wells
	 * @param next the starting index of the substring to be analyzed
	 * @return <B>true</B> if the substring begins with '0x' or '0X', and the next digit
	 * is a hexadecimal digit (0-9A-Fa-f); <B>false</B> otherwise
	 */
	public boolean isHexValue( int next ) {
		boolean result = false;
		
		try {
			boolean startsWithZero = cExpression[next] == '0';
			boolean followedByX = cExpression[next+1] == 'x' || cExpression[next+1] == 'X';
			boolean followedByHexDigit = isHexChar( cExpression[next+2] );
			
			result = startsWithZero && followedByX && followedByHexDigit;
		} catch ( ArrayIndexOutOfBoundsException e ) {} 
		
		return result;
	}
	
	/**
	 * Tests whether the substring at the given index is a valid symbol
	 * @author Aaron Wells
	 * @param next the starting index of the substring to be analyzed
	 * @return <B>true</B> if the substring begins with an upper- or lowercase
	 * character or an underscore; <B>false</B> otherwise
	 */
	public boolean isSymbolValue( int next ) {
		boolean result = false;
		
		try {
			if ( (""+cExpression[next]).matches("[A-Za-z_]") )
				result = true;
		} catch ( ArrayIndexOutOfBoundsException e ) {}
		
		return result;
	}
	
	/**
	 * Tests whether the substring at the given index is a valid function
	 * @param next the starting index of the substring to be analyzed
	 * @return <B>true</B> if the substring begins with A-Z, a-z, or underscore
	 * (_), followed by a string of zero or more symbol characters (A-Za-z0-9_);
	 *  <B>false</B> otherwise
	 */
	public boolean isFunctionValue( int next ) {
		boolean result = false;
		int position = next;
		char nextChar = cExpression[position];
		
		try {
			if ( !isSymbolValue(next) )
				throw new IllegalArgumentException();
			while ( isSymbolChar(nextChar) )
				nextChar = cExpression[++position];
			if ( nextChar == '(' )
				result = true;
		} catch ( IllegalArgumentException e ) {}
		
		return result;
	}
	
//	INDEX FINDERS

	/**
	 * Finds the closing parenthesis from the position of its
	 * corresponding opening parenthesis
	 * @author Aaron Wells
	 * @param from the index of the opening parenthesis
	 * @return the index of the closing parenthesis if it exists; the
	 * expression length if the closing parenthesis does not exist
	 * @throws IllegalArgumentException if character at <B>from</B> is not '('
	 */	
	public int findClosingParenthesis( int from ) {
		int pos = from;
		int left = 0, right = 0;
		
		if ( cExpression[pos] != '(' )
			throw new IllegalArgumentException("The index given does not mark a '(' character");
		
		while (  pos < cExpression.length ) {
			char c = cExpression[pos];
			if ( c == '(' ) left++;
			if ( c == ')' ) right++;
			if ( left == right ) break;
			pos++;
		}
		
		return pos;
	}

	/**
	 * Finds the index of the last symbol character in the substring beginning 
	 * with the given index
	 * @author Aaron Wells
	 * @param from the index where the substring begins
	 * @return the index of the last symbol character in the substring
	 * @throws IllegalArgumentException if substring at <B>from</B> is not a
	 * valid symbol value
	 */
	public int findLastSymbolChar( int from ) {
		int result = from;
		char nextChar = cExpression[result];
		
		if ( !isSymbolValue(result) )
			throw new IllegalArgumentException("The substring at the given"
					+ " index is not a valid symbol value");
		
		try {
			while ( isSymbolChar(nextChar) )
				nextChar = cExpression[++result];
		} catch ( ArrayIndexOutOfBoundsException e ) {}
		result = result - 1;
		
		return result;
	}

	/**
	 * Finds the index of the last decimal digit in the substring beginning 
	 * with the given index
	 * @author Aaron Wells
	 * @param from the index where the substring begins
	 * @return the index of the last decimal digit in the substring
	 * @throws IllegalArgumentException if substring at <B>from</B> is not a
	 * valid decimal value
	 */
	public int findLastDecimalDigit( int from ) {
		int result = from;
		char nextChar = cExpression[result];
		
		if ( !isDecimalValue(result) )
			throw new IllegalArgumentException("The substring at the given"
					+ " index is not a valid decimal value");
		
		try {
			while ( (""+nextChar).matches("\\d") )
				nextChar = cExpression[++result];
		} catch ( ArrayIndexOutOfBoundsException e ) {}
		result = result - 1;
		
		return result;
	}

	/**
	 * Finds the last digit in the hexadecimal substring beginning with the
	 * given index
	 * @author Aaron Wells
	 * @param from the index where the substring begins
	 * @return the index of the last hexadecimal digit in the substring
	 * @throws IllegalArgumentException if substring at <B>from</B> is not a
	 * valid hexadecimal value
	 */
	public int findLastHexDigit(int from) {
		int result = from + 2;
		
		boolean foundLastDigit = false;
		
		if ( !isHexValue(from) )
			throw new IllegalArgumentException("The substring at the given"
					+ " index is not a valid hexadecimal value");
		
		while ( !foundLastDigit ) {
			try {
				if ( isHexChar(cExpression[result+1]) )
					result++;
				else
					foundLastDigit = true;
			} catch ( ArrayIndexOutOfBoundsException e ) {foundLastDigit = true;}
		}
		
		return result;
	}

	/**
	 * Finds the position of the last digit in the binary substring beginning
	 * with the given index
	 * @author Aaron Wells
	 * @param from the index where the substring begins
	 * @return the index of the last binary digit in the substring
	 * @throws IllegalArgumentException if substring at <B>from</B> is not a
	 * valid binary value
	 */
	public int findLastBinaryDigit( int from ) {
		int result = from + 2;
		char nextChar = cExpression[result];
		
		if ( !isBinaryValue(from) )
			throw new IllegalArgumentException("The substring at the given"
					+ " index is not a valid binary value");
		
		try {
			while ( isBinaryChar(nextChar) )
				nextChar = cExpression[++result];
		} catch ( ArrayIndexOutOfBoundsException e ) {}
		result = result - 1;
		
		return result;
	}
	
	/**
	 * Returns this receiver's original string expression
	 * @author Aaron Wells
	 * @return the original string passed into the one argument String
	 * constructor
	 */
	public String getOriginalExpression() {
		return oExpression;
	}
	
	/**
	 * Returns this receiver's stripped string expression
	 * @author Aaron Wells
	 * @return the original string expression, less all whitespace characters
	 */
	public String getStringExpression() {
		return sExpression;
	}
	
	/**
	 * Returns the characters from this receiver's stripped string expression
	 * @author Aaron Wells
	 * @return a character array consisting of the characters in the stripped
	 * string expression in the same sequential order
	 * @throws NullPointerException if this receiver's character expression
	 * is null
	 */
	public char[] getCharacterExpression() {
		return Arrays.copyOf(cExpression, cExpression.length);
	}
	
	private static String strip(String str) {
		return str.replaceAll("\\s", "");
	}
	
	@Override
	public String toString() {
		return "ExpressionUtils [originalExpression=" + oExpression
				+ ", strippedExpression=" + sExpression
				+ ", characterExpression="
				+ Arrays.toString(cExpression) + "]";
	}

}
