package edu.uweo.javaintro.homework.url.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import edu.uweo.javaintro.homework.url.NameValuePair;

public class NameValuePairTest {
	
	static final String 	SINGLE_NAME = "key",
							SINGLE_VALUE = "value",
							SINGLE_NAME2 = "key2",
							SINGLE_VALUE2 = "value2",
							SINGLE_EXPECTED = "key=value";

	@Test
	public void constructorStringString() {
		NameValuePair pair = new NameValuePair(SINGLE_NAME, SINGLE_VALUE);
		assertEquals(SINGLE_EXPECTED, pair.toString());
	}
	
	@Test
	public void getList() {
		@SuppressWarnings("serial")
		Map<String,String> map = new HashMap<String,String>() {{
			put(SINGLE_NAME, SINGLE_VALUE);
			put(SINGLE_NAME2, SINGLE_VALUE2);
		}};
		@SuppressWarnings("serial")
		List<NameValuePair> expected = new ArrayList<NameValuePair>() {{
			add( NameValuePair.getInstance(SINGLE_NAME2+"="+SINGLE_VALUE2) );
			add( NameValuePair.getInstance(SINGLE_NAME+"="+SINGLE_VALUE) );
		}};
		assertEquals( expected, NameValuePair.getList(map));
	}
	
	@Test
	public void getInstance() {
		NameValuePair expected = new NameValuePair(SINGLE_NAME, SINGLE_VALUE);
		assertEquals(expected, NameValuePair.getInstance(SINGLE_NAME+"="+SINGLE_VALUE));
	}

}
