package edu.uweo.java2.homework.threads;

public class TaskThreaderTest {

	public static void main(String[] args) throws InterruptedException {
		TaskThreader threader = new TaskThreader(5000);
		threader.setDebug(false);
		Thread processThread = new Thread(threader::process);
		processThread.start();
		Thread.sleep(10000);
		TaskGenerator.initiateShutdown();
		processThread.join();
		threader.allThreadsAndTasksCompleted();
	}

}
