package edu.uweo.javaintro.homework.url.test;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.uweo.javaintro.homework.url.URLEncoder;

public class URLEncoderTest {

	@Test
	public void encodeFullURL() {
		URLEncoder encoder = new URLEncoder();
		encoder.setProtocol( "file" );
		encoder.setDomain( "mfg.acme.com" );
		encoder.setPort(80);
		encoder.setPath( "base/input/list.txt" );
		encoder.addQueryPair( "a", "b" );
		encoder.addQueryPair( "c", "d" );
		encoder.setFragmentID( "GROCERY" );
		
		String encodedURL = "file://mfg.acme.com:80/base/input/list.txt?a=b&c=d#GROCERY";
		assertEquals( encodedURL, encoder.encode() );
	}
	
	@Test
	public void encodeWtihNoProtocol() {
		URLEncoder encoder = new URLEncoder();
//		encoder.setProtocol( "file" );
		encoder.setDomain( "mfg.acme.com" );
		encoder.setPort(80);
		encoder.setPath( "base/input/list.txt" );
		encoder.addQueryPair( "a", "b" );
		encoder.addQueryPair( "c", "d" );
		encoder.setFragmentID( "GROCERY" );
		
		String encodedURL = "mfg.acme.com:80/base/input/list.txt?a=b&c=d#GROCERY";
		assertEquals( encodedURL, encoder.encode() );
	}
	
	@Test
	public void encodeWithNoDomain() {
		URLEncoder encoder = new URLEncoder();
		encoder.setProtocol( "file" );
//		encoder.setDomain( "mfg.acme.com" );
		encoder.setPort(80);
		encoder.setPath( "base/input/list.txt" );
		encoder.addQueryPair( "a", "b" );
		encoder.addQueryPair( "c", "d" );
		encoder.setFragmentID( "GROCERY" );
		
		String encodedURL = "file:///base/input/list.txt?a=b&c=d#GROCERY";
		assertEquals( encodedURL, encoder.encode() );
	}
	
	@Test
	public void encodeWithNoPort() {
		URLEncoder encoder = new URLEncoder();
		encoder.setProtocol( "file" );
		encoder.setDomain( "mfg.acme.com" );
//		encoder.setPort(80);
		encoder.setPath( "base/input/list.txt" );
		encoder.addQueryPair( "a", "b" );
		encoder.addQueryPair( "c", "d" );
		encoder.setFragmentID( "GROCERY" );
		
		String encodedURL = "file://mfg.acme.com/base/input/list.txt?a=b&c=d#GROCERY";
		assertEquals( encodedURL, encoder.encode() );
	}
	
	@Test
	public void encodeWithNoPath() {
		URLEncoder encoder = new URLEncoder();
		encoder.setProtocol( "file" );
		encoder.setDomain( "mfg.acme.com" );
		encoder.setPort(80);
//		encoder.setPath( "base/input/list.txt" );
		encoder.addQueryPair( "a", "b" );
		encoder.addQueryPair( "c", "d" );
		encoder.setFragmentID( "GROCERY" );
		
		String encodedURL = "file://mfg.acme.com:80/?a=b&c=d#GROCERY";
		assertEquals( encodedURL, encoder.encode() );
	}
	
	@Test
	public void encodeWithNoQueryString() {
		URLEncoder encoder = new URLEncoder();
		encoder.setProtocol( "file" );
		encoder.setDomain( "mfg.acme.com" );
		encoder.setPort(80);
		encoder.setPath( "base/input/list.txt" );
//		encoder.addQueryPair( "a", "b" );
//		encoder.addQueryPair( "c", "d" );
		encoder.setFragmentID( "GROCERY" );
		
		String encodedURL = "file://mfg.acme.com:80/base/input/list.txt#GROCERY";
		assertEquals( encodedURL, encoder.encode() );
	}
	
	@Test
	public void encodeWithNoFragmentID() {
		URLEncoder encoder = new URLEncoder();
		encoder.setProtocol( "file" );
		encoder.setDomain( "mfg.acme.com" );
		encoder.setPort(80);
		encoder.setPath( "base/input/list.txt" );
		encoder.addQueryPair( "a", "b" );
		encoder.addQueryPair( "c", "d" );
//		encoder.setFragmentID( "GROCERY" );
		
		String encodedURL = "file://mfg.acme.com:80/base/input/list.txt?a=b&c=d";
		assertEquals( encodedURL, encoder.encode() );
	}
	
	@Test
	public void encodeOverloaded() {
		URLEncoder encoder = new URLEncoder();
//		encoder.setProtocol( "file" );
//		encoder.setDomain( "mfg.acme.com" );
//		encoder.setPort(80);
//		encoder.setPath( "base/input/list.txt" );
		encoder.addQueryPair( "a", "b" );
		encoder.addQueryPair( "c", "d" );
//		encoder.setFragmentID( "GROCERY" );
		
		String encodedURL = "file://mfg.acme.com:80/base/input/list.txt?a=b&c=d#GROCERY";
		String actual = encoder.encode("file", 80, "mfg.acme.com", "base/input/list.txt", "GROCERY");
		assertEquals( encodedURL, actual );
	}
	
	@Test
	public void encodeOverloadedWithPresets() {
		URLEncoder encoder = new URLEncoder();
		encoder.setProtocol( "fummp" );
		encoder.setDomain( "magoo.acme.com" );
		encoder.setPort(80);
		encoder.setPath( "ardent/input/list.txt" );
		encoder.addQueryPair( "a", "b" );
		encoder.addQueryPair( "c", "d" );
		encoder.setFragmentID( "MOCKERY" );
		
		String encodedURL = "file://mfg.acme.com:80/base/input/list.txt?a=b&c=d#GROCERY";
		String actual = encoder.encode("file", 80, "mfg.acme.com", "base/input/list.txt", "GROCERY");
		assertEquals( encodedURL, actual );
	}
	
	@Test
	public void encodeWithQueriesToEncode() {
		URLEncoder encoder = new URLEncoder();
		encoder.setProtocol( "file" );
		encoder.setDomain( "mfg.acme.com" );
		encoder.setPort(80);
		encoder.setPath( "base/input/list.txt" );
		encoder.addQueryPair( "a 5", "b!blue" );
		encoder.addQueryPair( "c,c", "d+moo" );
		encoder.setFragmentID( "GROCERY" );
		
		String encodedURL = "file://mfg.acme.com:80/base/input/list.txt?a+5=b%21blue&c%2Cc=d%2Bmoo#GROCERY";
		assertEquals( encodedURL, encoder.encode() );
	}

}
