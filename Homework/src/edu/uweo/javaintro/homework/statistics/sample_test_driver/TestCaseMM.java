package edu.uweo.javaintro.homework.statistics.sample_test_driver;

import java.util.Arrays;
import edu.uweo.javaintro.homework.statistics.Statistics;

public class TestCaseMM
{
    private double[]    dataSet_;
    private double      expectedMean_;
    private double      expectedMedian_;
    
    /* Test case for mean and median. */
    public TestCaseMM( double[] dataSet, double mean, double median )
    {
        dataSet_ = Arrays.copyOf( dataSet, dataSet.length );
        expectedMean_ = mean;
        expectedMedian_ = median;
    }
    
    public void test()
    {
    	double epsilon = 0.0001;
        Statistics  stats           = new Statistics( dataSet_ );
        double      actualMean      = stats.mean();
        double      actualMedian    = stats.median();
        if ( actualMean > expectedMean_ + epsilon ||
        		actualMean < expectedMean_ - epsilon )
        	throw new Error( "actual mean " + actualMean + 
        			" exceeds epsilon bounds for " +
        			expectedMean_ + " +-" + epsilon
        			);
        
        if ( actualMedian > expectedMedian_ + epsilon ||
        		actualMedian < expectedMedian_ - epsilon )
        	throw new Error( "actual median " + actualMedian +
        			" exceeds epsilon bounds for " +
        			expectedMedian_ + " +-" + epsilon
        			);
        
        System.out.println( "mean: expected " + expectedMean_ +
        		"\tactual " + actualMean);
        System.out.println( "median: expected " + expectedMedian_ +
        		"\tactual " + actualMedian);
        System.out.println( "OKAY" );
        System.out.println();
        /*
         * Are the actual mean and median equal to the expected values (use
         * an epsilon test!)? If not, create a descriptive error message, then
         * "throw new Error( descriptiveErrorMessage )." Or, if you are 
         * unhappy with exceptions, print the error message then call 
         * System.exit(). Whether the test 
         * fails or not, this is a good place to print out diagnostics. It's
         * also a good place to put a breakpoint in the Eclipse debugger.
         */
    }
}
