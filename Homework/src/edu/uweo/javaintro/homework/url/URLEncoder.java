package edu.uweo.javaintro.homework.url;

import java.util.List;
import java.util.Map;


public class URLEncoder extends URLManager {
	
	private URLQueryEncoder queryEncoder;
	
	public URLEncoder() {
		queryEncoder = new URLQueryEncoder();
	}

	public String encode() {
		String protocol = getProtocol();
		String domain = getDomain();
		String path = getPath();
		int port = getPort();
		String queryString = queryEncoder.encode();
		String fragmentId = getFragmentID();
		boolean hasPath = hasPath(),
				hasQueryString = hasQueryString(),
				hasFragmentId = ( fragmentId != null );
		
		StringBuilder url = new StringBuilder();
		
		if ( hasProtocol() )
			url.append(protocol + PROTOCOL_DELIM );
		if ( hasDomain() ) {
			url.append(domain);
			if ( hasPort() ) {
				url.append(PORT_DELIM);
				url.append( port );
			}
		}
		if ( hasPath || hasQueryString || hasFragmentId )
				url.append(PATH_DELIM);
		if ( hasPath )
			url.append(path);
		if ( hasQueryString )
			url.append( QUERY_DELIM + queryString );
		if ( hasFragmentId )
			url.append( FRAGMENT_DELIM + fragmentId );
		
		return url.toString();
	}
	
	public String encode(
			String protocol, int port, String domain,
			String path, String fragmentID ) {
		setProtocol(protocol);
		setPort(port);
		setDomain(domain);
		setPath(path);
		setFragmentID(fragmentID);
		
		return encode();
	}
	
	public void setQueryPairs( List<NameValuePair> queryPairs ) {
		queryEncoder.setQueryPairs( queryPairs );
	}
	
	public void setQueryPairs( Map<String,String> queryPairs ) {
		queryEncoder.setQueryPairs( NameValuePair.getList(queryPairs) );
	}
	
	public List<NameValuePair> getQueryPairs() {
		return queryEncoder.getQueryPairs();
	}
	
	public void addQueryPair( String name, String value ) {
		queryEncoder.addQueryPair( name, value );
	}
	
	public void addQueryPair( NameValuePair pair ) {
		queryEncoder.addQueryPair(pair);
	}
	
	private boolean hasQueryString() {
		String encoded = queryEncoder.encode();
		return ( encoded != null ) &&
				( encoded.length() > 0 );
	}

	@Override
	public String getQueryString() {
		return queryEncoder.encode();
	}
	
}
