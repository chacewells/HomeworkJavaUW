package edu.uweo.javaintro.homework.url;

import java.util.Map;
import java.util.function.Consumer;

public class URLDecoder extends URLManager {
	
	public static final String	PROTOCOL_DELIM_REGEX	= PROTOCOL_DELIM,
								DOMAIN_DELIM_REGEX		= "([" + PORT_DELIM + PATH_DELIM + "]|$)",
								PORT_DELIM_REGEX		= "(" + PATH_DELIM + "|$)",
								PATH_DELIM_REGEX		= "([" + QUERY_DELIM + FRAGMENT_DELIM + "]|$)",
								QUERY_DELIM_REGEX		= "(" + FRAGMENT_DELIM + "|$)",
								PROTOCOL_REGEX = "\\w+",
								DOMAIN_REGEX = "(\\w+\\.)*\\w+",
								PORT_REGEX = "\\d+",
								PATH_REGEX = "((\\w+\\.)*\\w+/)*((\\w+\\.)*\\w+/?)+",
								QUERY_REGEX = "(.+\\=.+&)*.+\\=.+";
	
	private URLQueryDecoder queryDecoder;
	private String queryString;
	private String toDecode;
	
	/**
	 * Constructs this decoder and initializes the URL to be decoded.
	 * @param toDecode - the URL to be decoded
	 */
	public URLDecoder( String toDecode ) {
		this.toDecode = toDecode;
		queryDecoder = new URLQueryDecoder("");
	}
	
	/**
	 * Parses the receiver's URL string into its component parts. Components
	 * can be retrieved by using the superclass's getter methods.
	 */
	public void decode() {
		String str = toDecode;
		
		str = splitParse( str, PROTOCOL_DELIM_REGEX, PROTOCOL_REGEX, this::setProtocol );
		str = splitParse( str, DOMAIN_DELIM_REGEX, DOMAIN_REGEX, this::setDomain );
		str = splitParse( str, PORT_DELIM_REGEX, PORT_REGEX, this::setPort );
		str = splitParse( str, PATH_DELIM_REGEX, PATH_REGEX, this::setPath );
		str = splitParse( str, QUERY_DELIM_REGEX, QUERY_REGEX, this::setQueryString );
		
		setFragmentID( str.length() > 1 ? str : null );
	}
	
	/**
	 * Parses the first part of a URL substring and assigns the value to the
	 * appropriate field in this URLDecoder. Truncates the URL substring
	 * up to and including the first instance of the delimiter and returns
	 * the remaining portion.
	 * @param str - the URL string/substring from which to parse
	 * @param rDelims - the delimiter(s)
	 * @param rContent - a pattern against which the parsed component will be checked
	 * @param setActivity - specifies the setter that consumes the parsed URL component
	 * @return the substring to the right of the delimiter
	 */
	private String splitParse( String str, String rDelims, String rContent, Consumer<String> setActivity ) {
		String result = null;
		String[] split = str.split( rDelims, 2 );
		if ( 
				( split.length > 1
				&& 
				split[0].matches( rContent ) )
				) {
			setActivity.accept( split[0] );
			result = split[1];
		} else if ( str.matches( rContent + rDelims ) ) {
			setActivity.accept( str );
			result = "";
		} else {
			setActivity.accept( null );
			result = str;
		}
		
		return result;
	}
	
	private void setPort(String port) {
		int iPort = -1;
		try {
			iPort = Integer.parseInt( (String)port );
		} catch (NumberFormatException e) {
			iPort = -1;
		}
		
		setPort(iPort);
	}
	
	/**
	 * Returns a query decoder for manipulating the query string
	 * @return this receiver's URLQueryDecoder helper
	 */
	public URLQueryDecoder getQueryDecoder() {
		return queryDecoder;
	}
	
	/**
	 * Returns the raw query string parsed from this receiver's URL.
	 * @return The last raw query string parsed from this receiver's URL; null if URL has not yet been decoded.
	 */
	@Override
	public String getQueryString() {
		return queryString;
	}
	
	private void setQueryString( String queryString ) {
		this.queryString = queryString;
	}
	
	/**
	 * Returns a map of names and values parsed and decoded from this receiver's query string.
	 * @return A map representing names and values if the URL has been decoded; an empty map otherwise.
	 */
	public Map<String, String> getQueryMap() {
		queryDecoder = new URLQueryDecoder( getQueryString() );
		return queryDecoder.decode();
	}
	
}
