package edu.uweo.java2.homework.serialize;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Trim {

    public static final String FILE_IN = "to_trim.ser";
    public static final String FILE_OUT = "trimmed.ser";
    
    private List<SerializableObject> list;
    private List<Integer> toTrim;

	private String fileIn;
	private String fileOut;

	public static void main(String[] args) throws IOException, ClassNotFoundException {
    	
//    	create a Trim instance and read the lists
    	Trim t = new Trim(FILE_IN, FILE_OUT);
    	t.read();
    	
//    	write out the trimmed list
    	t.write();
    	
//    	verify results
    	List<SerializableObject> results;
    	try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(FILE_OUT))) {
			results = (List<SerializableObject>) in.readObject();
    	}
    	
    	for ( SerializableObject o : results.subList(0, 10) )
    		System.out.println(o);
    	
    	System.out.println("Number of SerializableObject instances in list: " +
    						results.size());
    }
    
    public Trim(String fileName) {
    	this.fileIn = fileName;
    }
    
    public Trim(String fileIn, String fileOut) {
    	this(fileIn);
    	this.fileOut = fileOut;
    }
    
    @SuppressWarnings("unchecked")
	public void read() throws ClassNotFoundException {
    	try (ObjectInputStream in = new ObjectInputStream(
    			new BufferedInputStream(new FileInputStream(fileIn)))) {
	    	list = (List<SerializableObject>) in.readObject();
	    	toTrim = (List<Integer>) in.readObject();
	    	in.close();
    	} catch ( IOException e ) {
    		e.printStackTrace();
    		System.exit(1);
    	}
    }
    
    public void write(String fileOut) {
    	this.fileOut = fileOut;
    	write();
    }
    
    public void write() {
    	try (ObjectOutputStream out = 
    			new ObjectOutputStream(new FileOutputStream(fileOut))) {
    		List<SerializableObject> trimmed = trimmedList();
			out.writeObject(trimmed);
		} catch ( Exception e ) {
			throw new RuntimeException(e);
		}
    }
    
    public void read(String fileName) throws ClassNotFoundException {
    	this.fileIn = fileName;
    	read();
    }
    
    public List<SerializableObject> fullList() {
    	return list;
    }
    
    public List<SerializableObject> trimmedList() {
		List<SerializableObject> trimmed = new ArrayList<>();
		for (SerializableObject obj : list) {
			if ( toTrim.contains(obj.hashCode()) )
				continue;
			trimmed.add(obj);
		}
		return trimmed;
    }
    
    public List<Integer> getHashCodes() {
    	return toTrim;
    }
    
}